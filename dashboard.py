"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'transportila.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            _('Administration'),
            column=1,
            collapsible=True,
            css_classes=('collapse closed grp-closed',),
            models=(
                'django.contrib.*',
                'accounts.models.CustomUser',
                'accounts.models.UserConfirmation',
                'accounts.models.NewUserPassword',
                'accounts.models.Attachment',
                'oauth2_provider.*',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Profiles'),
            column=1,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'profiles.models.BasicInfo',
                'profiles.models.DocumentInfo',
                'profiles.models.BillingInfo',
                'company.*'
            ),
        ))

        self.children.append(modules.ModelList(
            _('Points'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed grp-closed',),
            models=('points.*',),
        ))

        self.children.append(modules.ModelList(
            _('Wiki'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed grp-closed',),
            models=('wiki.*',),
        ))

        self.children.append(modules.ModelList(
            _('Vehicles'),
            collapsible=True,
            column=2,
            css_classes=('collapse closed',),
            models=('vehicles.*',),
        ))

        self.children.append(modules.ModelList(
            _('Places'),
            column=2,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'places.models.Airport',
                'places.models.Port',
                'places.models.Warehouse',
                'places.models.Customs',
                'places.models.Place',
                'places.models.ServicePlace',
                'places.models.Service',
                'places.models.TypeService',
                'places.models.TypeServiceTypePlace',
                'places.models.TypePlace',
                'places.models.Company',
                'places.models.CompanyType',
                'places.models.PlaceCompany',
                'places.models.Country',
                'places.models.City',
                'places.models.Address',
                'places.models.WorkTime',
                'places.models.Phone',
                'places.models.Email',
                'places.models.Site',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Payments'),
            column=2,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'payments.models.*',
            ),
        ))
        self.children.append(modules.ModelList(
            _('Auxiliaries'),
            column=2,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'auxiliaries.models.*',
            ),
        ))

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))
        self.children.append(modules.ModelList(
            _('Orders'),
            column=3,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'orders.models.*',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Transportations'),
            column=3,
            collapsible=True,
            css_classes=('collapse closed',),
            models=(
                'transportations.models.*',
            ),
        ))





#group example
# append a group for "Administration" & "Applications"
# self.children.append(modules.Group(
#     _('Group: Administration & Applications'),
#     column=1,
#     collapsible=True,
#     children = [
#         modules.AppList(
#             _('Administration'),
#             column=1,
#             collapsible=False,
#             models=('django.contrib.*',),
#         ),
#         modules.AppList(
#             _('Applications'),
#             column=1,
#             css_classes=('collapse closed',),
#             exclude=('django.contrib.*',),
#         )
#     ]
# ))
