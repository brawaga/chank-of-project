from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope

from .models import OrderPaymentInfo
from .serializers import OrderPaymentInfoSerializer

__author__ = 'sandan'


class OrderPaymentInfoViewSet(ModelViewSet):
    model = OrderPaymentInfo
    serializer_class = OrderPaymentInfoSerializer
    queryset = OrderPaymentInfo.objects.all()
    permission_classes = (permissions.IsAdminUser, TokenHasReadWriteScope)

    # def list(self, request, *args, **kwargs):
    #     pass