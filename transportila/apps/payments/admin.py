from django.contrib.admin import ModelAdmin, register

from .models import Currency, OrderPaymentInfo


@register(Currency)
class AdminCurrency(ModelAdmin):
    list_display = ['slug', 'title']
    prepopulated_fields = {'slug': ('title',)}


@register(OrderPaymentInfo)
class AdminTransportationPaymentInfo(ModelAdmin):
    list_display = ['cost_transportation', 'pre_payment', 'post_payment', 'payment_method_type', 'currency', 'order']


