from django.conf.urls import patterns, url, include
from rest_framework import routers

from .api import OrderPaymentInfoViewSet

__author__ = 'sandan'

v1_router = routers.SimpleRouter()
v1_router.register(r'payments', OrderPaymentInfoViewSet)

v1_urlpatterns = [
    url(r'', include(v1_router.urls))
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]
