from support.serializers import CustomModelSerializer
from .models import Currency, OrderPaymentInfo
from rest_framework.serializers import SlugRelatedField, SerializerMethodField, IntegerField
from auxiliaries.serializers import ChoicesIntegerField
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class CurrencySerializer(CustomModelSerializer):
    class Meta:
        model = Currency
        fields = ('slug', 'title')


class CurrencyRelatedField(SlugRelatedField):
    def to_representation(self, obj):
        return CurrencySerializer(obj).data


class PaymentMethodTypeChoiceField(IntegerField):
    def to_representation(self, value):
        return dict(id=value, title=OrderPaymentInfo.PAYMENT_METHOD_TYPE[value][1])


class OrderPaymentInfoSerializer(CustomModelSerializer):
    currency = CurrencyRelatedField(queryset=Currency.objects.all(), slug_field='slug')
    payment_method_type = PaymentMethodTypeChoiceField()

    class Meta:
        model = OrderPaymentInfo
        fields = ('cost_transportation', 'pre_payment', 'post_payment', 'payment_method_type', 'currency')
        depth = 1

    def validate_cost_transportation(self, cost_transportation):
        if cost_transportation <= 0:
            raise ApiStandartException(dict(error_code=400079))
        return cost_transportation

    def validate_per_payment(self, pre_payment):
        if pre_payment <= 0:
            raise ApiStandartException(dict(error_code=400080))
        return pre_payment

    def validate_post_payment(self, post_payment):
        if post_payment <= 0:
            raise ApiStandartException(dict(error_code=400081))
        return post_payment

    def create(self, validated_data):
        return OrderPaymentInfo.objects.create(**validated_data)
