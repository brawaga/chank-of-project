from django.contrib.admin import ModelAdmin, register
from genericadmin.admin import GenericTabularInline
from sorl.thumbnail import get_thumbnail

from .models import Characteristic, CharacteristicUnit, Comment, Attachment, Color, Feedback
from modeltranslation.admin import TranslationAdmin


@register(Characteristic)
class AdminCharacteristic(TranslationAdmin):
    list_display = ['slug', 'title']
    prepopulated_fields = {'slug': ('title',)}


@register(CharacteristicUnit)
class AdminCharacteristicUnit(TranslationAdmin):
    list_display = ['title', 'id']


@register(Comment)
class AdminComment(ModelAdmin):
    list_display = ['id', 'commentator', 'text', 'created_at']


@register(Attachment)
class AdminAttachment(ModelAdmin):
    list_display = ['url', 'created_at', 'id']


class InlineAttachment(GenericTabularInline):
    classes = ('grp-collapse grp-open',)
    fieldsets = (
        (
            None,
            {
                'fields': ('url', 'small_image', 'position', 'user')
            }
        ),
    )
    readonly_fields = ['small_image']
    sortable_field_name = 'position'
    model = Attachment
    extra = 0

    def small_image(self, obj):
        try:
            image = get_thumbnail(obj.path, '60x60', format='PNG')
            return '<img src="'+ str(image.url) +'"/>'
        except:
            return None

    small_image.short_description = "Image"
    small_image.allow_tags = True


@register(Color)
class AdminColor(TranslationAdmin):
    list_display = ['title']


@register(Feedback)
class AdminFeedback(ModelAdmin):
    pass