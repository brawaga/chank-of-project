from django.http import HttpResponse
from rest_framework import permissions

from support.helpers import FieldFullness
from support.exceptions import ApiStandartException
from profiles.models import BasicInfo, DocumentInfo

__author__ = 'sandan'


class IsOrderCreator(permissions.BasePermission):
    """
    Permission check for create order.
    """

    def has_permission(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            if not request.user.has_perm('orders.add_order'):
                raise ApiStandartException(dict(error_code=400036))

            # if FieldFullness(BasicInfo.get_by_user(request.user)).get_fullness() != 100 or \
            #     FieldFullness(DocumentInfo.get_by_user(request.user)).get_fullness() != 100:
            #     raise ApiStandartException(dict(error_code=400074))
        return True


class BetPermissions(permissions.BasePermission):
    """
    Permission check for create bet.
    """

    def has_permission(self, request, view):
        if request.method == 'POST':
            if not request.user.has_perm('orders.add_bet'):
                raise ApiStandartException(dict(error_code=400038))
            if FieldFullness(BasicInfo.get_by_user(request.user)).get_fullness() != 100 or \
                FieldFullness(DocumentInfo.get_by_user(request.user)).get_fullness() != 100:
                raise ApiStandartException(dict(error_code=400074))
            return True
        if request.method in ('PUT', 'DELETE'):
            if request.user.has_perms(['orders.delete_bet', 'orders.change_bet']):
                return True
            else:
                raise ApiStandartException(dict(error_code=400042))
        return True


class TransportationPermissions(permissions.BasePermission):
    """
    Permission check for transportation.
    """

    def has_permission(self, request, view):
        if request.user.is_transporter():
            return True
        else:
            raise ApiStandartException(dict(error_code=400061))
