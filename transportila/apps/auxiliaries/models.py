from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model, SlugField, CharField, ForeignKey, DateTimeField, URLField, \
    PositiveSmallIntegerField, BooleanField, UUIDField, EmailField, TextField
from django.db.models.fields import PositiveIntegerField
from django.utils.translation import ugettext_lazy as _

from support.exceptions import ApiStandartException
from accounts.models import CustomUser


class CharacteristicUnit(Model):
    title = CharField(max_length=256, verbose_name=_(u'title'))

    class Meta:
        verbose_name = _(u'characteristic unit')
        verbose_name_plural = _(u'characteristics units')

    def __unicode__(self):
        return u'Characteristic unit {0}'.format(self.title)


class Characteristic(Model):
    UNIT_TYPES = (
        (u'integer', u'integer'),
        (u'float', u'float'),
        (u'string', u'string'),
        (u'boolean', u'boolean'),
    )

    slug = SlugField(primary_key=True, unique=True, db_index=True, verbose_name=_(u'slug'))
    title = CharField(max_length=256, verbose_name=_(u'title'))
    unit = ForeignKey(CharacteristicUnit, blank=True, null=True, verbose_name=_(u'unit'))
    unit_type = CharField(choices=UNIT_TYPES, max_length=64, verbose_name=_(u'unit type'))
    is_main = BooleanField(default=False, verbose_name=_(u'is main'))

    class Meta:
        verbose_name = _(u'characteristic')
        verbose_name_plural = _(u'characteristics')

    def __unicode__(self):
        return u'characteristic - {0}'.format(self.slug)

    @classmethod
    def get_for_cart(cls, cart):
        return cls.objects.filter()

    @classmethod
    def get_object(cls, characteristic_id):
        try:
            return cls.objects.get(pk=characteristic_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400011))


class Comment(Model):
    text = CharField(max_length=2048, verbose_name=_(u'text'))
    commentator = ForeignKey(CustomUser, verbose_name=_(u'commentator'))
    commenting_content_type = ForeignKey(ContentType, related_name='commenting', blank=True, null=True,
                                         verbose_name=_(u'commenting content type'))
    commenting_object_id = PositiveIntegerField(blank=True, null=True,
                                                verbose_name=_(u'commenting object id'))
    commenting_content_object = GenericForeignKey('commenting_content_type', 'commenting_object_id')
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'comment')
        verbose_name_plural = _(u'comments')

    def __unicode__(self):
        return u'comment "{0}" from {1}'.format(self.text[:100], self.commentator.email)


class TransporterReview(Model):

    MARK_TYPES = (
        (0, _(u'bad')),
        (1, _(u'good'))
    )

    text = CharField(max_length=2048, verbose_name=_(u'text'))
    commentator = ForeignKey(CustomUser, verbose_name=_(u'commentator'))
    order_id = UUIDField(verbose_name=_(u'order_id'))
    user = ForeignKey(CustomUser, related_name='reviews', verbose_name=_(u'user'))
    mark = PositiveSmallIntegerField(choices=MARK_TYPES, verbose_name=_(u'mark'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'comment')
        verbose_name_plural = _(u'comments')

    def __unicode__(self):
        return u'comment "{0}" from {1}'.format(self.text[:100], self.commentator.email)


class Attachment(Model):
    ATTACHMENT_GROUPS = (
        (0, _(u'customer')),
        (1, _(u'driver')),
        (2, _(u'dispatcher')),
    )

    url = URLField(verbose_name=_(u'url'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))
    position = PositiveSmallIntegerField(default=100, verbose_name=_(u'position'))
    user = ForeignKey(CustomUser, blank=True, null=True, verbose_name=_(u'user'))

    content_type = ForeignKey(ContentType, blank=True, null=True, verbose_name=_(u'content type'))
    object_id = PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    object_uuid = CharField(max_length=128, blank=True, null=True, verbose_name=_(u'object_uuid'))
    uuid_content_object = GenericForeignKey('content_type', 'object_uuid')

    class Meta:
        verbose_name = _(u'attachment')
        verbose_name_plural = _(u'attachments')
        ordering = ['position']

    def __unicode__(self):
        return u'attachment - {0}'.format(self.url)

    @classmethod
    def get_object(cls, attachment_id, user):
        try:
            return cls.objects.get(id=attachment_id, user=user)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400008))

    def set_content_type(self, item):
        self.content_type = ContentType.objects.get_for_model(item)
        self.object_id = item.id
        self.save()

    def set_uuid_content_type(self, item):
        self.content_type = ContentType.objects.get_for_model(item)
        self.object_uuid = item.uuid
        self.save()

    @classmethod
    def get_by_id(cls, pk):
        try:
            return cls.objects.get(pk=pk)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400008))


class Color(Model):
    title = CharField(max_length=128, verbose_name=_(u'title'))

    class Meta:
        verbose_name = _(u'color')
        verbose_name_plural = _(u'colors')

    def __unicode__(self):
        return u'color - {0}'.format(self.title)


class Feedback(Model):
    email = EmailField(verbose_name=u'email')
    message = TextField(max_length=4096, verbose_name=u'message')

    class Meta:
        verbose_name = _(u'feedback')
        verbose_name_plural = _(u'feedback')

    def __unicode__(self):
        return u'email - {0}, message - {1}'.format(self.email, self.message[0:50])
