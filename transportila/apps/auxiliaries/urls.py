from django.conf.urls import patterns, url, include
from rest_framework import routers

from .api import ColorView, FeedbackView, UploadView

__author__ = 'sandan'

v1_router = routers.SimpleRouter()

v1_urlpatterns = [
    url(r'accounts/upload/$', UploadView.as_view()),
    url(r'colors/$', ColorView.as_view()),
    url(r'feedback/$', FeedbackView.as_view()),
    url(r'', include(v1_router.urls))
]


urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
]
