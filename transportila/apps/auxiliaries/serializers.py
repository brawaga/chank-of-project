from django.contrib.contenttypes.models import ContentType
from rest_framework.serializers import DateTimeField, SerializerMethodField, SlugRelatedField, CharField, IntegerField

from support.serializers import CustomModelSerializer, CustomSerializer
from .models import CharacteristicUnit, Characteristic, Attachment, Color, Feedback
from accounts.models import CustomUser
from vehicles.models import Vehicle, Cart
from profiles.models import DocumentInfo

__author__ = 'sandan'


class ChoicesIntegerField(IntegerField):
    def to_representation(self, value):
        return dict(id=value, title=eval('self.parent.instance.get_{0}_display()'.format(self.field_name)))


class CustomDateTimeField(DateTimeField):
    def to_representation(self, value):
        return value.strftime("%Y-%m-%dT%H:%M")


class CharacteristicUnitSerializer(CustomModelSerializer):
    class Meta:
        model = CharacteristicUnit
        fields = ('id', 'title',)


class CharacteristicSerializer(CustomModelSerializer):
    unit_title = SerializerMethodField()

    def get_unit_title(self, characteristic):
        return characteristic.unit.title if characteristic.unit else None

    class Meta:
        model = Characteristic
        fields = ('slug', 'title', 'unit_title', 'is_main', 'unit_type',)


class CharacteristicCreateSerializer(CustomSerializer):
    characteristic = SlugRelatedField(queryset=Characteristic.objects.all(), slug_field='slug')
    value = CharField()


class AttachmentShortSerializer(CustomModelSerializer):
    class Meta:
        model = Attachment
        fields = ('id', 'url')


class ColorSerializer(CustomModelSerializer):
    class Meta:
        model = Color
        fields = ('id', 'title')


class FeedbackSerializer(CustomModelSerializer):
    class Meta:
        model = Feedback

