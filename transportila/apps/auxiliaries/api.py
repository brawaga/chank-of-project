from django.conf import settings
from rest_framework.views import APIView
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework import permissions
from rest_framework.response import Response
from queue.file_upload import FileUploader
from repositories import s3
from repositories.s3 import S3Repo

from .models import Color, Attachment
from .serializers import ColorSerializer, FeedbackSerializer, AttachmentShortSerializer
from queue.send_email import SendEmail

__author__ = 'sandan'


class ColorView(APIView):
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get(self, request):
        return Response(ColorSerializer(Color.objects.all(), many=True).data)


class FeedbackView(APIView):
    permission_classes = ()

    def post(self, request):
        serializer = FeedbackSerializer(data=request.data)
        serializer.is_valid()
        feedback = serializer.save()
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.FEEDBACK_LETTER_SUBJECT,
                    template_text=settings.FEEDBACK_LETTER_TXT,
                    template_html=settings.FEEDBACK_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[serializer.validated_data.get('email')],
                    params=dict(
                        file_server_url=settings.FILE_SERVER_URL
                    )
                )
            ))

        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.ADMIN_FEEDBACK_LETTER_SUBJECT,
                    template_text=settings.ADMIN_FEEDBACK_LETTER_TXT,
                    template_html=settings.ADMIN_FEEDBACK_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=settings.ADMINS,
                    params=dict(
                        feedback=feedback,
                        file_server_url=settings.FILE_SERVER_URL
                    )
                )
            ))
        return Response()


class UploadView(APIView):
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def post(self, request):
        file_ = request.data['file']
        subdir = S3Repo.get_subdir()
        filename = S3Repo.get_filename(file_.content_type)
        FileUploader.upload.delay({'data': {'s3_subdir': subdir,
                                            'filename': filename,
                                            'content-type': file_.content_type,
                                            'content_as_file': request.data['file']}})

        url = s3.S3_BUCKET_URL + subdir + filename

        attachment = Attachment.objects.create(
            url=url,
            user=request.user
        )
        return Response(AttachmentShortSerializer(attachment).data)
