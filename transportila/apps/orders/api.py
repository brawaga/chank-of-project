# coding=utf-8
import datetime

from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework import permissions
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework import status
import pytz

from .models import Order, CategoryCargoCharacteristic, CategoryCargo, TransportationType, Bet
from .serializers import OrderSerializer, CategoryCargoCharacteristicSerializer, CategoryCargoSerializer, \
    SortedCategoryCargoSerializer, OrderDetailSerializer, TransportationTypeSerializer, OrderSearchSerializer, \
    OrderMobileShortSerializer, OrderWebShortSerializer, BetSerializer, BetCreateSerializer, \
    RequestCustomerAcceptBetSerializer, RequestListBetSerializer, ListBetSerializer, \
    RequestTransporterAcceptanceBetSerializer, RequestResendOrderCertificates
from auxiliaries.serializers import CharacteristicSerializer
from auxiliaries.permissions import IsOrderCreator, BetPermissions
from support.exceptions import ApiStandartException
from vehicles.models import Vehicle
from transportations.models import Transportation
from queue.send_certificate import SendingTransportationCertificates

__author__ = 'sandan'


class CategoryCargoViewSet(ModelViewSet):
    model = CategoryCargo
    serializer_class = CategoryCargoSerializer
    queryset = CategoryCargo.objects.select_related('parent').prefetch_related(
        'transportation_types',
        'characteristics').all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        return Response(SortedCategoryCargoSerializer(self.model.sort_by_parent(self.get_queryset()), many=True).data)

    @detail_route(methods=['get'])
    def characteristics(self, request, *args, **kwargs):
        return Response(CharacteristicSerializer(self.get_object().characteristics.all().order_by(
            'category_cargo_characteristics__position'), many=True).data)


class TransportationTypeViewSet(ModelViewSet):
    model = TransportationType
    serializer_class = TransportationTypeSerializer
    queryset = TransportationType.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        if 'category_cargo' in request.query_params:
            queryset = queryset.filter(
                transportation_types_category_cargo__category_cargo__slug=request.query_params['category_cargo']
            )
        return Response(self.serializer_class(queryset, many=True).data)


class CategoryCargoCharacteristicViewSet(ModelViewSet):
    model = CategoryCargoCharacteristic
    serializer_class = CategoryCargoCharacteristicSerializer
    queryset = CategoryCargoCharacteristic.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)


class OrdersPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class OrderViewSet(ModelViewSet):
    model = Order
    serializer_class = OrderDetailSerializer
    queryset = Order.objects.prefetch_related('o_bettors', 'o_bettors__order_bets').all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope, IsOrderCreator)
    pagination_class = OrdersPagination

    def create(self, request, *args, **kwargs):
        serializer = OrderSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        order = serializer.create(serializer.validated_data)
        return Response(OrderDetailSerializer(order, context=dict(request=request)).data,
                        status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        return Response(OrderDetailSerializer(self.get_object(), context=dict(request=request)).data)

    def list(self, request, *args, **kwargs):
        serializer = OrderSearchSerializer(data=request.query_params, context=dict(queryset=self.get_queryset(),
                                                                                   request=request))
        serializer.is_valid()
        queryset = serializer.context['queryset'].prefetch_related(
            'category_cargo_transportation_type',
            'category_cargo_transportation_type__category_cargo',
            'category_cargo_transportation_type__transportation_type',
            'order_items'
        ).distinct().order_by('-date_published')

        paginator = self.pagination_class()
        result_pages = paginator.paginate_queryset(queryset, request)
        preview_type = serializer.validated_data.get('preview_type', None)
        if preview_type == 0:
            return paginator.get_paginated_response(
                OrderWebShortSerializer(result_pages, many=True, context=dict(request=request)).data
            )
        elif preview_type == 1:
            return paginator.get_paginated_response(
                OrderMobileShortSerializer(result_pages, many=True, context=dict(request=request)).data
            )

    def update(self, request, *args, **kwargs):
        #todo добавить задачу в rabbitmq check_valid_order, если заказ публикуется
        pass


class BetsPagination(PageNumberPagination):
    page_size = 20


class BetsViewSet(ModelViewSet):
    model = Bet
    serializer_class = BetSerializer
    queryset = Bet.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope, BetPermissions)
    pagination_class = BetsPagination

    def get_queryset(self):
        return self.get_queryset().filter(is_active=True)

    def get_object(self):
        bet = Bet.get_by_pk(self.kwargs['pk'])
        bet.check_state(self.request.user)
        return bet

    def list(self, request, *args, **kwargs):
        serializer = RequestListBetSerializer(data=request.query_params)
        serializer.is_valid()
        if not serializer.validated_data.get('order'):
            raise ApiStandartException(dict(error_code=400100))
        return Response(ListBetSerializer(serializer.validated_data.get('order').get_bets(),
                                          context=dict(request=request), many=True).data)

    def create(self, request, *args, **kwargs):
        serializer = BetCreateSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        bet = serializer.create(serializer.validated_data)
        return Response(self.serializer_class(bet, context=dict(request=request)).data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        bet = self.get_object()
        user = request.user
        if bet.status == 0 and user.is_customer:
            # customer accept transporter or dispatcher bet
            #todo additional checking
            serializer = RequestCustomerAcceptBetSerializer(data=request.query_params)
            serializer.is_valid()
            bet.set_favorite_status(serializer.validated_data.get('expired_time'))
            return Response()

        elif bet.status == 1 and (user.is_dispatcher() or user.is_transporter()):
            # transporter or dispatcher accept order for transportation. Only point to point and one transporter.
            if user.is_transporter():
                serializer = RequestTransporterAcceptanceBetSerializer(
                    data=request.query_params, context=dict(order=bet.order_bettor.order))
                serializer.is_valid()
                if bet.vehicle:
                    vehicle_carts = list(bet.vehicle.vehicle_carts.all())
                    if not (bet.vehicle.user == request.user and list(bet.carts.all()) == vehicle_carts):
                        raise ApiStandartException(dict(error_code=400045))
                    transporter = bet.vehicle
                else:
                    transporter = request.user
                Transportation.making_from_customer_order_for_single_transporter(
                    transporter, bet.order_bettor.order,
                    serializer.validated_data.get('date_acceptance')
                )
                bet.set_winner_status()
                Bet.decline_losers(bet.order_bettor.order)
                return Response()
            else:
                raise ApiStandartException(dict(error_code=400047))
        else:
            raise ApiStandartException(dict(error_code=400043))

    def destroy(self, request, *args, **kwargs):
        bet = self.get_object()
        user = request.user
        if bet.status == 0:
            if user.is_customer():
                # customer destroy bet at review level
                comment = request.data['comment'] if 'comment' in request.data else None
                bet.set_declined_by_customer_status(comment)
                return Response()
            elif user.is_transporter() or user.is_dispatcher():
                # transporter or dispatcher destroy bet at review level
                bet.set_canceled_status()
                return Response()
        elif bet.status == 1:
            if user.is_customer():
                # customer destroy bet at favorite level
                bet.set_declined_by_customer_status()
                return Response()
            elif user.is_transporter or user.is_dispatcher:
                # transporter or dispatcher destroy bet at favorite level
                bet.set_declined_by_transporter_status()
                return Response()


class ResendCertificateView(APIView):
    permission_classes = ()
    serializer_class = RequestResendOrderCertificates

    def get(self, request):
        if not request.user.is_customer():
            raise ApiStandartException(dict(error_code=400071))
        serializer = self.serializer_class(data=request.query_params, context=dict(request=request))
        serializer.is_valid()
        try:
            transportation = Transportation.objects.get(
                order_content_type=ContentType.objects.get_for_model(Order),
                order_object_id=serializer.validated_data['order'].uuid
            )
        except Transportation.DoesNotExist:
            raise ApiStandartException(dict(error_code=400070))
        SendingTransportationCertificates.send.delay(transportation.uuid)
        return Response()



