# coding=utf-8
import datetime
import uuid

from django.conf import settings
from django.db.models import Model, CharField, IntegerField, ForeignKey, PositiveIntegerField, DateTimeField, \
    BooleanField, EmailField, SlugField, FloatField, UUIDField, DateField, ManyToManyField, OneToOneField, Q
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
import pytz
from django.utils.encoding import force_unicode

from vehicles.models import Characteristic, Vehicle, Cart
from accounts.models import CustomUser
from places.models import Address
from support.exceptions import ApiStandartException
from payments.models import OrderPaymentInfo, Currency
from points.models import Point
from points.serializers import PointShortSerializer, PointDetailSerializer
from support.push import send_bet_approve_push, send_bet_decline_push
from queue.send_email import SendEmail


class Trustee(Model):
    email = EmailField(max_length=256, verbose_name=_(u'email'))
    first_name = CharField(max_length=256, verbose_name=_(u'first name'))
    last_name = CharField(max_length=256, verbose_name=_(u'last name'))
    phone = CharField(max_length=24, verbose_name=_(u'phone'))
    social_id = CharField(max_length=256, verbose_name=_(u'social id or iin'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'agent')
        verbose_name_plural = _(u'agents')

    def __unicode__(self):
        return u'agent - {0} {1}, email - {2}'.format(self.first_name, self.last_name, self.email)


class TransportationType(Model):
    slug = SlugField(primary_key=True, unique=True, db_index=True, verbose_name=_(u'slug'))
    title = CharField(max_length=256, verbose_name=_(u'title'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'transportation type')
        verbose_name_plural = _(u'transportation types')

    def __unicode__(self):
        return u'category transportation type - {0}'.format(self.title)

    @classmethod
    def get_by_slug(cls, slug):
        try:
            return cls.objects.get(slug=slug)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400029))


class CategoryCargo(Model):
    slug = SlugField(max_length=256, unique=True, primary_key=True, verbose_name=u'category slug')
    title = CharField(max_length=256, verbose_name=_(u'title'))
    parent = ForeignKey('self', blank=True, null=True, verbose_name=_(u'parent category'))
    transportation_types = ManyToManyField(TransportationType, through='CategoryCargoTransportationType',
                                           blank=True, verbose_name=_(u'transportation type'))
    characteristics = ManyToManyField(Characteristic, blank=True, through='CategoryCargoCharacteristic',
                                      verbose_name=_(u'characteristics'))

    position = PositiveIntegerField(default=0, verbose_name=_(u'position'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'category cargo')
        verbose_name_plural = _(u'categories cargo')
        ordering = ['position']

    def __unicode__(self):
        return u'category cargo - {0}'.format(self.slug)

    @classmethod
    def sort_by_parent(cls, categories):
        categories = sorted(list(categories), key=lambda category: category.parent)
        sorted_categories = list()
        for item in categories:
            if not item.parent:
                sorted_categories.append(
                    dict(
                        parent=item,
                        childs=list()
                    )
                )
            else:
                for index, sorted_item in enumerate(sorted_categories):
                    if sorted_item['parent'] == item.parent:
                        sorted_categories[index]['childs'].append(item)
                        break
        for index, item in enumerate(sorted_categories):
            item['childs'] = sorted(item['childs'], key=lambda category: category.position)
        sorted_categories = sorted(sorted_categories, key=lambda s_category: s_category['parent'].position)
        return sorted_categories

    @classmethod
    def get_by_slug(cls, slug):
        try:
            return cls.objects.get(slug=slug)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400028))


class CategoryCargoTransportationType(Model):
    category_cargo = ForeignKey(CategoryCargo, verbose_name=_(u'category cargo'))
    transportation_type = ForeignKey(TransportationType, related_name='transportation_types_category_cargo',
                                     verbose_name=_(u'transportation type'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'category cargo transportation type')
        verbose_name_plural = _(u'categories cargo transportation types')
        unique_together = ('category_cargo', 'transportation_type')

    def __unicode__(self):
        return u'category cargo transportation type - {0}'.format(self.id)


class CategoryCargoCharacteristic(Model):
    category_cargo = ForeignKey(CategoryCargo, verbose_name=_(u'category cargo'))
    characteristic = ForeignKey(Characteristic, related_name='category_cargo_characteristics',
                                verbose_name=_(u'characteristic'))
    position = PositiveIntegerField(default=0, verbose_name=_(u'position'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'category cargo characteristic')
        verbose_name_plural = _(u'category cargo characteristics')
        unique_together = ('category_cargo', 'characteristic')
        ordering = ['position']

    def __unicode__(self):
        return u'category cargo characteristic {0} - {1}'.format(self.category_cargo.title, self.characteristic.title)

    @classmethod
    def check_main_characteristics(cls, characteristics_list, category_cargo):
        main_characteristics = list(Characteristic.objects.filter(
            category_cargo_characteristics__category_cargo=category_cargo,
            is_main=True
        ))
        characteristics = [item['characteristic'] for item in characteristics_list]
        difference = set(main_characteristics).difference(characteristics)
        if difference:
            raise ApiStandartException(dict(error_code=400066))


class Order(Model):
    ORDER_TYPES = (
        (0, _(u'fixed rate')),
        (1, _(u'auction')),
    )

    ORDER_STATUSES = (
        (0, _(u'draft')),
        (1, _(u'active')),
        (2, _(u'auction is canceled')),
        (3, _(u'auction is finished')),
        (4, _(u'carried out')),
        (5, _(u'delivered')),
        (6, _(u'auction is completed, are expected to confirm transporters')),
    )

    uuid = UUIDField(primary_key=True, unique=True, db_index=True, verbose_name=_(u'uuid'))
    title = CharField(max_length=256, blank=True, null=True, verbose_name=_(u'title'))
    description = CharField(max_length=512, blank=True, null=True, verbose_name=_(u'description'))
    customer = ForeignKey(CustomUser, related_name='order_customers', verbose_name=_(u'customer'))
    category_cargo_transportation_type = ForeignKey(CategoryCargoTransportationType, blank=True, null=True,
                                                    verbose_name=_(u'category cargo transportation type'))
    order_type = PositiveIntegerField(choices=ORDER_TYPES, verbose_name=_(u'order type'))
    valid_until = DateTimeField(blank=True, null=True, verbose_name=_(u'valid until'))
    is_published = BooleanField(default=False, verbose_name=_(u'is published'))
    payment_info = OneToOneField(OrderPaymentInfo, verbose_name=_(u'payment info'))
    cost_cargo = FloatField(verbose_name=_(u'cost cargo'))
    cost_cargo_currency = ForeignKey(Currency, verbose_name=_(u'cost cargo currency'))
    with_items = BooleanField(default=False, verbose_name=_(u'with items'))
    status = PositiveIntegerField(choices=ORDER_STATUSES, blank=True, verbose_name=_(u'status'))
    distance = PositiveIntegerField(default=0, verbose_name=_(u'distance'))
    date_accepted = DateTimeField(blank=True, null=True, verbose_name=_(u'date accepted'))
    date_completed = DateTimeField(blank=True, null=True, verbose_name=_(u'date completed'))
    date_published = DateTimeField(blank=True, null=True, verbose_name=_(u'date published'))
    date_created = DateTimeField(auto_now_add=True, verbose_name=_(u'date created'))

    class Meta:
        verbose_name = _(u'order')
        verbose_name_plural = _(u'orders')
        ordering = ['-date_created']

    def __unicode__(self):
        return u'order - {0}'.format(self.uuid)

    @classmethod
    def get_by_uuid(cls, uuid):
        try:
            return cls.objects.get(uuid=uuid)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400100))

    def get_common_address(self, user, order_item_point):
        from .serializers import PointUserSerializer
        is_limited_access = user.check_order_limited_access(self)
        if self.category_cargo_transportation_type.transportation_type.slug == 'point-to-point':
            point_type = None
            for item in OrderItemPoint.POINTS_TYPES:
                if item[1].lower() == order_item_point.point_content_type.model.lower():
                    point_type = item[0]
                    break

            if point_type == 0:
                if not order_item_point.point_user or is_limited_access:
                    try:
                        return order_item_point.point_content_object.get_limited(
                            is_limited_access
                        )
                    except AttributeError:
                        return None
                else:
                    return PointUserSerializer(order_item_point.point_user).data
        return None

    def set_auction_is_canceled_status(self):
        self.status = 2
        self.save()

    def set_carried_out_status(self):
        self.status = 4
        self.date_accepted = datetime.datetime.utcnow()
        self.save()

    def set_delivered_status(self):
        self.status = 5
        self.save()
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.ORDER_WAS_DELIVERED_LETTER_SUBJECT,
                    template_text=settings.ORDER_WAS_DELIVERED_LETTER_TXT,
                    template_html=settings.ORDER_WAS_DELIVERED_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[self.customer.email],
                    params=dict(
                        order_title=self.title,
                        file_server_url=settings.FILE_SERVER_URL,
                    )
                )
            )
        )

    def check_carried_out_status(self):
        if self.status == 4:
            return True
        return False

    def get_bets(self):
        # bets = self.o_bettors.order_bets.exclude(status__in=Bet.get_not_active_statuses())

        bets = list(Bet.objects.filter(order_bettor__order=self).exclude(status__in=Bet.get_not_active_statuses()).select_related('order_bettor').prefetch_related(
            'order_bettor__bettor').order_by('-created_at'))
        formatted_bets = list()
        for bet in bets:
            bettor_rec = None
            for item in formatted_bets:
                if item['bettor'] == bet.order_bettor.bettor:
                    bettor_rec = item
                    break
            if bettor_rec is None:
                bettor_rec = dict(
                    bettor=bet.order_bettor.bettor,
                    bets=list()
                )
                formatted_bets.append(bettor_rec)
            bettor_rec['bets'].append(bet)
        return formatted_bets

    @classmethod
    def get_customer_actual_statuses(cls):
        return [3, 4]

    @classmethod
    def get_transporter_actual_statuses(cls):
        return [1]

    @classmethod
    def get_transporter_mine_actual_statuses(cls):
        return [1, 4]


class OrderBettor(Model):
    bettor = ForeignKey(CustomUser, related_name='b_orders', verbose_name=_(u'bettor'))
    order = ForeignKey(Order, related_name='o_bettors', verbose_name=_(u'order'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'order bettor')
        verbose_name_plural = _(u'order bettors')
        unique_together = ('bettor', 'order')

    def __unicode__(self):
        return u'order bettor {0}'.format(self.id)


class Bet(Model):
    BETS_STATUSES = (
        (0, _(u'review')),
        (1, _(u'favorite')),
        (2, _(u'winner')),
        (3, _(u'declined')),
        (4, _(u'declined by customer')),
        (5, _(u'declined by transporter')),
        (6, _(u'not active')),
        (7, _(u'canceled')),
        (8, _(u'annulled')),
    )

    cost = FloatField(verbose_name=_(u'amount'))
    comment = CharField(max_length=2048, blank=True, null=True, verbose_name=_(u'comment'))
    order_bettor = ForeignKey(OrderBettor, related_name='order_bets', verbose_name=_(u'order bettor'))
    vehicle = ForeignKey(Vehicle, blank=True, null=True, verbose_name=_(u'vehicle'))
    acceptance_at = DateTimeField(blank=True, null=True, verbose_name=_(u'acceptance at'))
    expired_at = DateTimeField(blank=True, null=True, verbose_name=_(u'expired at'))
    carts = ManyToManyField(Cart, blank=True, verbose_name=_(u'carts'))
    cancelled_at = DateTimeField(blank=True, null=True, verbose_name=_(u'cancelled at'))
    cancelled_user = ForeignKey(CustomUser, blank=True, null=True, verbose_name=_(u'cancelled user'))
    cancelled_comment = CharField(max_length=2048, blank=True, null=True, verbose_name=_(u'cancelled comment'))
    status = PositiveIntegerField(choices=BETS_STATUSES, default=BETS_STATUSES[0][0], verbose_name=_(u'status'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'bet')
        verbose_name_plural = _(u'bets')

    def __unicode__(self):
        return u'order bet cost - {0}'.format(self.cost)

    def set_favorite_status(self, expired_time):
        from queue.check_expired_bet import CheckExpiredBet
        self.status = 1
        self.expired_at = datetime.datetime.utcnow() + datetime.timedelta(
            hours=expired_time.hour,
            minutes=expired_time.minute
        )
        self.save()
        send_bet_approve_push(self)
        CheckExpiredBet.check.apply_async(
            (self.id,),
            eta=self.expired_at
        )
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.CUSTOMER_SET_FAVORITE_BET_LETTER_SUBJECT,
                    template_text=settings.CUSTOMER_SET_FAVORITE_BET_LETTER_TXT,
                    template_html=settings.CUSTOMER_SET_FAVORITE_BET_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[self.order_bettor.bettor.email],
                    params=dict(
                        order_title=self.order_bettor.order.title,
                        file_server_url=settings.FILE_SERVER_URL,
                    )
                )
            )
        )

    def check_favorite_status(self):
        if self.status == 1:
            return True
        return False

    def set_winner_status(self):
        self.status = 2
        self.save()

    @classmethod
    def get_winner_status(self):
        return 2

    def check_winner_status(self):
        if self.status == 2:
            return True
        return False

    def set_declined_status(self):
        self.status = 3
        self.save()
        # send_bet_decline_push(self, message="Заказчик выбрал другого перевозчика", title="Ставка отклонена")

    def check_declined_status(self):
        if self.status == 3:
            return True
        return False

    def set_declined_by_customer_status(self, comment=None):
        self.status = 4
        self.cancelled_at = datetime.datetime.utcnow()
        self.cancelled_comment = comment
        self.save()
        # todo translate when will adding language to registration
        send_bet_decline_push(self, message=force_unicode(_(u'Заказчик отклонил вашу ставку')),
                              title=force_unicode(_(u'Ставка отклонена')))

    def check_declined_by_customer_status(self):
        if self.status == 4:
            return True
        return False

    def set_declined_by_transporter_status(self):
        self.status = 5
        self.cancelled_at = datetime.datetime.utcnow()
        self.save()
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.TRANSPORTER_DECLINE_BET_LETTER_SUBJECT,
                    template_text=settings.TRANSPORTER_DECLINE_BET_LETTER_TXT,
                    template_html=settings.TRANSPORTER_DECLINE_BET_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[self.order_bettor.order.customer.email],
                    params=dict(
                        order_title=self.order_bettor.order.title,
                        transporter_nickname=self.order_bettor.bettor.nickname,
                        file_server_url=settings.FILE_SERVER_URL
                    )
                )
            ))

    def check_declined_by_transporter_status(self):
        if self.status == 5:
            return True
        return False

    def set_not_active_status(self):
        self.status = 6
        self.save()

    def check_not_active_status(self):
        if self.status == 6:
            return True
        return False

    def set_canceled_status(self):
        self.status = 7
        self.is_active = False
        self.cancelled_at = datetime.datetime.utcnow()
        self.save()

    def check_canceled_status(self):
        if self.status == 7:
            return True
        return False

    def set_annulled_status(self):
        self.status = 8
        self.save()

    def check_annulled_status(self):
        if self.status == 8:
            return True
        return False

    @classmethod
    def get_active_statuses(cls):
        return [0, 1, 2, 3, 4, 5, 6, 8]

    @classmethod
    def get_not_active_statuses(cls):
        return [7]

    @classmethod
    def get_active_statuses_for_transporter(cls):
        return [0, 1, 2]

    @classmethod
    def decline_losers(cls, order):
        bets = list(Bet.objects.filter(Q(Q(status=0) | Q(status=1)), order_bettor__order=order))
        for item in bets:
            item.set_declined_status()

    @classmethod
    def get_by_pk(cls, pk, only_active=True):
        try:
            if only_active:
                return cls.objects.get(pk=pk, status__in=cls.get_active_statuses())
            else:
                return cls.objects.get(pk=pk)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400049))

    def check_state(self, user):
        if not (user == self.order_bettor.bettor or user == self.order_bettor.order.customer):
            raise ApiStandartException(dict(error_code=400041))
        if not (self.status == 0 or self.status == 1):
            raise ApiStandartException(dict(error_code=400043))
        if self.status == 8 or self.expired_at and pytz.UTC.localize(datetime.datetime.now()) > self.expired_at:
            raise ApiStandartException(dict(error_code=400046))


class OrderDate(Model):
    ORDER_DATE_TYPES = (
        (0, _(u'to')),
        (1, _(u'between')),
        (2, _(u'on')),
        (3, _(u'after')),
    )

    first_date = DateTimeField(blank=True, null=True, verbose_name=_(u'first date'))
    second_date = DateTimeField(blank=True, null=True, verbose_name=_(u'second date'))
    order_date_type = PositiveIntegerField(choices=ORDER_DATE_TYPES, verbose_name=_(u'order date type'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'order date')
        verbose_name_plural = _(u'order dates')

    def __unicode__(self):
        return u'order date type - {0}, first_date - {1}, second_date - {2}'.format(
            self.ORDER_DATE_TYPES[self.order_date_type][1], self.first_date, self.second_date)

    def check_correctness(self, date_comparison, valid_until):
        if (self.order_date_type == 0 and (date_comparison < valid_until or date_comparison > self.first_date)) or \
                (self.order_date_type == 1 and (date_comparison < self.first_date or date_comparison > self.second_date)) or \
                (self.order_date_type == 2 and self.first_date != date_comparison) or \
                (self.order_date_type == 3 and date_comparison < self.first_date):
            raise ApiStandartException(dict(error_code=400059))
        return True

    @classmethod
    def check_transfer_order_item_date(cls, date_acceptance, date_delivery, valid_until):
        cls.validate_order_date(date_acceptance, valid_until)
        cls.validate_order_date(date_delivery, valid_until)
        cls.validate_acceptance_and_delivery_dates(date_acceptance, date_delivery)

    @classmethod
    def validate_order_date(cls, order_date, valid_until):
        if (order_date['order_date_type'] == 0 or order_date['order_date_type'] == 3) and order_date['first_date'] < valid_until:
            raise ApiStandartException(dict(error_code=400075))

        if order_date['order_date_type'] == 3:
            if order_date['first_date'] > order_date['second_date']:
                raise ApiStandartException(dict(error_code=400077))
            if order_date['first_date'] < valid_until or order_date['second_date'] < valid_until:
                raise ApiStandartException(dict(error_code=400075))

    @classmethod
    def validate_acceptance_and_delivery_dates(cls, date_acceptance, date_delivery):
        if date_acceptance['order_date_type'] == 0 or date_acceptance['order_date_type'] == 3:
            if date_acceptance['first_date'] > date_delivery['first_date']:
                raise ApiStandartException(dict(error_code=400076))

        if date_acceptance['order_date_type'] == 1:
            if date_acceptance['second_date'] > date_delivery['first_date']:
                raise ApiStandartException(dict(error_code=400076))


class PointUser(Model):
    address = CharField(max_length=1024, verbose_name=_(u'user address'))
    latitude = FloatField(verbose_name=_(u'latitude'))
    longitude = FloatField(verbose_name=_(u'longitude'))

    class Meta:
        verbose_name = _(u'point of user')
        verbose_name_plural = _(u'points of users')

    def __unicode__(self):
        return u'point of user {0}'.format(self.address)


class OrderItemPoint(Model):
    POINTS_TYPES = (
        (0, u'Address'),
    )

    TRUSTEE_TYPES = (
        (0, u'CustomUser'),
        (1, u'Trustee'),
    )

    date_transfer = ForeignKey(OrderDate, verbose_name=_(u'date transfer'))
    trustee_content_type = ForeignKey(ContentType, related_name='order_item_point_trustee', blank=True,
                                      null=True, verbose_name=_(u'trustee content type'))
    trustee_object_id = PositiveIntegerField(blank=True, null=True, verbose_name=_(u'trustee object id'))
    trustee_content_object = GenericForeignKey('trustee_content_type', 'trustee_object_id')

    point_content_type = ForeignKey(ContentType, related_name='order_item_point', blank=True, null=True,
                                    verbose_name=_(u'point content type'))
    point_object_id = PositiveIntegerField(blank=True, null=True,
                                           verbose_name=_(u'point object id'))
    point_content_object = GenericForeignKey('point_content_type', 'point_object_id')

    point_user = OneToOneField(PointUser, blank=True, null=True, verbose_name=_(u'point of user'))

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'order item point')
        verbose_name_plural = _(u'orders items points')

    def __unicode__(self):
        return u'order item point - {0}'.format(self.id)

    @classmethod
    def point_type_valid(cls, point_type):
        for item in cls.POINTS_TYPES:
            if item[0] == point_type:
                return True
        return False

    @classmethod
    def trustee_type_valid(cls, trustee_type):
        for item in cls.TRUSTEE_TYPES:
            if item[0] == trustee_type:
                return True
        return False


class OrderItem(Model):
    ORDER_ITEM_STATUSES = (
        (0, _(u'transportation is not confirmed')),
        (1, _(u'waiting for dispatch')),
        (2, _(u'shipment is on its way')),
        (3, _(u'delivered')),
    )
    uuid = UUIDField(unique=True, primary_key=True, db_index=True, verbose_name=_(u'uuid'))
    title = CharField(max_length=256, verbose_name=_(u'title'))
    description = CharField(max_length=2048, blank=True, null=True, verbose_name=_(u'description'))
    order = ForeignKey(Order, related_name='order_items', verbose_name=u'order')
    status = PositiveIntegerField(choices=ORDER_ITEM_STATUSES, default=0,
                                  verbose_name=_(u'status'))
    point_acceptance = OneToOneField(OrderItemPoint, related_name='point_acceptance',
                                     verbose_name=_(u'point acceptance'))
    point_delivery = OneToOneField(OrderItemPoint, related_name='point_delivery', verbose_name=_(u'point delivery'))

    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'order item')
        verbose_name_plural = _(u'order items')

    def __unicode__(self):
        return u'order item - {0}, order - {1}'.format(self.title, self.order.title)

    def set_waiting_dispatch_status(self):
        self.status = 1
        self.save()

    def is_waiting_dispatch_status(self):
        if self.status == 1:
            return True
        return False

    def set_shipment_is_on_its_way_status(self):
        self.status = 2
        self.save()

    def set_delivered_status(self):
        self.status = 3
        self.save()

    def get_coord(self, user):
        from transportations.models import TransportationCargoItem
        if self.status in [0, 1]:
            # return point acceptance coord
            if self.point_acceptance.point_content_type.model.lower() == 'address':
                try:
                    return dict(
                        point=dict(
                            latitude=self.point_acceptance.point_content_object.address_coord.latitude,
                            longitude=self.point_acceptance.point_content_object.address_coord.longitude,
                            created_at=self.point_acceptance.created_at.strftime("%Y-%m-%dT%H:%M")
                        ),
                        user=None
                    )
                except AttributeError:
                    return None
            return None
        elif self.status == 2:
            # return current transporter coord
            if self.order.customer != user:
                return None
            transportation_cargo_item = TransportationCargoItem.objects.select_related('transportation').filter(
                Q(cargo_item_object_id=self.pk,
                  cargo_item_content_type=ContentType.objects.get_for_model(self)) & Q(status__in=[1, 2, 3]) &
                Q(Q(parent=None) | Q(parent__status=4)) & Q(Q(child=None) | Q(child__status=0))
            ).first()
            transporter_type = transportation_cargo_item.transportation.transporter_trip.transporter_content_type.model.lower()
            if transporter_type == 'customuser':
                return PointDetailSerializer(
                    dict(
                        point=Point.get_latest_for_user(
                            transportation_cargo_item.transportation.transporter_trip.transporter_content_object
                        ),
                        user=transportation_cargo_item.transportation.transporter_trip.transporter_content_object
                    )
                ).data
            elif transporter_type == 'vehicle':
                return PointDetailSerializer(
                    dict(
                        point=Point.get_latest_for_user(
                            transportation_cargo_item.transportation.transporter_trip.transporter_content_object.user
                        ),
                        user=transportation_cargo_item.transportation.transporter_trip.transporter_content_object.user
                    )
                ).data
        elif self.status == 3:
            # return point delivery coord
            if self.order.customer != user:
                return None
            if self.point_delivery.point_content_type.model.lower() == 'address':
                try:
                    return dict(
                        point=dict(
                            latitude=self.point_delivery.point_content_object.address_coord.latitude,
                            longitude=self.point_delivery.point_content_object.address_coord.longitude,
                            created_at=self.point_delivery.created_at.strftime("%Y-%m-%dT%H:%M")
                        ),
                        user=None
                    )
                except AttributeError:
                    return None
            return None
        return None


class OrderItemCategoryCargoCharacteristic(Model):
    order_item = ForeignKey(OrderItem, related_name='order_item_characteristics', verbose_name=_(u'order item'))
    category_cargo_characteristic = ForeignKey(CategoryCargoCharacteristic,
                                               verbose_name=_(u'category cargo characteristic'))
    value = CharField(max_length=512, verbose_name=_(u'value'))
    created_at = DateTimeField(auto_now_add=True, verbose_name=_(u'created at'))

    class Meta:
        verbose_name = _(u'order item category cargo characteristic')
        verbose_name_plural = _(u'order item category cargo characteristics')
        unique_together = ('order_item', 'category_cargo_characteristic')

    def __unicode__(self):
        return u'order item - {0}, category cargo characteristic - {1}, value - {2}'.format(
            self.order_item.title, self.category_cargo_characteristic.characteristic.title, self.value)

