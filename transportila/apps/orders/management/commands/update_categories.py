import json
from django.utils.text import slugify
import os
from django.core.management.base import BaseCommand

from orders.models import CategoryCargo, TransportationType, CategoryCargoTransportationType


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'profiles' app\n")
        with open(os.path.join(os.path.dirname(__file__), 'public_orders_categorycargo.json')) as data_file:
            data = json.load(data_file)
        with_parents = list()
        for item in data:
            print item
            if not item['parent_id']:
                with_parents.append(
                    dict(
                        parent=item,
                        childs=list()
                    )
                )

        for item in data:
            if item['parent_id']:
                for index, category in enumerate(with_parents):
                    if item['parent_id'] == category['parent']['slug']:
                        with_parents[index]['childs'].append(item)
                        break

        transportation_types = list()

        for item in ['Point - To - Point', 'Point - To - Multipoints', 'Multipoints - To - Multipoints', 'Multipoints - To - Point']:
            slug = slugify(item)
            transportation_types.append(TransportationType.objects.get_or_create(title=item, slug=slug)[0])

        for category in with_parents:
            parent, is_created = CategoryCargo.objects.get_or_create(slug=category['parent']['slug'], title=category['parent']['title'])
            for item in category['childs']:
                child, is_created = CategoryCargo.objects.get_or_create(slug=item['slug'], title=item['title'], parent=parent)
                for transportation_type in transportation_types:
                    CategoryCargoTransportationType.objects.create(category_cargo=child, transportation_type=transportation_type)

        self.stdout.write("Successfully complete\n")
