# coding=utf-8
import uuid
import datetime
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
import pytz
from rest_framework.serializers import SerializerMethodField, IntegerField, DictField, ListField, CharField, \
    SlugRelatedField, BooleanField, DateField, FloatField, PrimaryKeyRelatedField, TimeField, DateTimeField, UUIDField
from django.conf import settings

from support.serializers import CustomSerializer, CustomModelSerializer
from .models import CategoryCargo, Order, OrderDate, OrderItem, Trustee, OrderItemPoint, TransportationType, \
    CategoryCargoTransportationType, CategoryCargoCharacteristic, PointUser, OrderItemCategoryCargoCharacteristic, Bet, \
    OrderBettor
from places.models import Address
from support.exceptions import ApiStandartException
from auxiliaries.models import Characteristic
from auxiliaries.serializers import CustomDateTimeField, CharacteristicSerializer, CharacteristicCreateSerializer, \
    ChoicesIntegerField
from payments.serializers import OrderPaymentInfoSerializer, CurrencyRelatedField, CurrencySerializer
from payments.models import OrderPaymentInfo, Currency
from vehicles.models import Vehicle, Cart
from accounts.serializers import UserSerializer
from vehicles.serializers import CartShortSerializer, VehicleShortSerializer
from queue.check_valid_order import CheckValidOrder
from support.route import get_route_g
from queue.send_email import SendEmail


__author__ = 'sandan'


class TransportationTypeSerializer(CustomModelSerializer):
    class Meta:
        model = TransportationType
        fields = ('slug', 'title')


class TransportationTypeRelatedField(SlugRelatedField):
    def to_representation(self, obj):
        return TransportationTypeSerializer(obj).data


class CategoryCargoSerializer(CustomModelSerializer):
    transportation_types = TransportationTypeRelatedField(queryset=TransportationType.objects.all(), slug_field='slug',
                                                          many=True)

    class Meta:
        model = CategoryCargo
        fields = ('slug', 'title', 'parent', 'transportation_types',)


class CategoryCargoShortSerializer(CustomModelSerializer):
    class Meta:
        model = CategoryCargo
        fields = ('slug', 'title', 'parent')


class CategoryCargoDetailSerializer(CustomModelSerializer):
    transportation_types = TransportationTypeRelatedField(queryset=TransportationType.objects.all(), slug_field='slug',
                                                          many=True)
    parent = SerializerMethodField()

    def get_transportation_types(self, category):
        return TransportationTypeSerializer(category.transportation_types, many=True).data

    def get_parent(self, category):
        parent = None
        if category.parent:
            return CategoryCargoSerializer(category.parent).data
        return parent

    class Meta:
        model = CategoryCargo
        fields = ('slug', 'title', 'parent', 'transportation_types',)


class SortedCategoryCargoSerializer(CustomSerializer):
    parent = CategoryCargoSerializer()
    childs = CategoryCargoSerializer(many=True)


class CategoryCargoCharacteristicSerializer(CustomModelSerializer):
    pass


class OrderDateSerializer(CustomModelSerializer):
    first_date = CustomDateTimeField()
    second_date = CustomDateTimeField(required=False)

    class Meta:
        model = OrderDate
        fields = ('id', 'first_date', 'second_date', 'order_date_type')

    def validate(self, attrs):
        if attrs['order_date_type'] == 1 and attrs['second_date'] < attrs['first_date']:
            raise ApiStandartException(dict(error_code=400077))
        return attrs


class OrderDateDetailSerializer(CustomModelSerializer):
    first_date = CustomDateTimeField()
    second_date = CustomDateTimeField(required=False)
    order_date_type = SerializerMethodField()

    def get_order_date_type(self, order_date):
        return dict(id=order_date.order_date_type, title=order_date.get_order_date_type_display())

    class Meta:
        model = OrderDate
        fields = ('id', 'first_date', 'second_date', 'order_date_type')


class TrusteeSerializer(CustomModelSerializer):
    class Meta:
        model = Trustee
        fields = ('id', 'email', 'first_name', 'last_name', 'phone', 'social_id')


class OrderItemCategoryCargoCharacteristicSerializer(CustomModelSerializer):
    characteristic = SerializerMethodField()

    def get_characteristic(self, obj):
        return CharacteristicSerializer(obj.category_cargo_characteristic.characteristic).data

    class Meta:
        model = OrderItemCategoryCargoCharacteristic
        fields = ('value', 'characteristic')


class PointUserSerializer(CustomModelSerializer):
    class Meta:
        model = PointUser
        fields = ('id', 'address', 'latitude', 'longitude')


class OrderItemPointSerializer(CustomModelSerializer):
    trustee_type = IntegerField()
    trustee_data = DictField(child=CharField(), required=False)
    point_type = IntegerField()
    point_data = DictField(child=CharField())
    date_transfer = OrderDateSerializer()
    user_address = CharField(required=False)

    class Meta:
        model = OrderItemPoint
        fields = ('id', 'user_address', 'trustee_type', 'trustee_data', 'point_type', 'point_data', 'date_transfer')

    def validate_point_type(self, point_type):
        if OrderItemPoint.point_type_valid(point_type):
            self.point_type = point_type
            return point_type
        else:
            raise ApiStandartException(dict(error_code='400026'))

    def validate_point_data(self, point_data):
        if self.point_type == 0:
            point_user_serializer = PointUserSerializer(data=point_data)
            point_user_serializer.is_valid()
            point_data = point_user_serializer.validated_data
        return point_data

    def validate_trustee_type(self, trustee_type):
        if OrderItemPoint.trustee_type_valid(trustee_type):
            self.trustee_type = trustee_type
            return trustee_type
        else:
            raise ApiStandartException(dict(error_code='400027'))

            # todo исправить типы доверенных лиц, добавить отдельный тип отображения для мобильной версии

    def validate_trustee_data(self, trustee_data):
        if self.trustee_type == 0:
            return None
        if self.trustee_type == 1:
            trustee_serializer = TrusteeSerializer(data=trustee_data)
            trustee_serializer.is_valid()
            return trustee_serializer.validated_data


class OrderItemPointDetailSerializer(CustomModelSerializer):
    date_transfer = SerializerMethodField()
    trustee_type = SerializerMethodField()
    trustee_data = SerializerMethodField()
    point_type = SerializerMethodField()
    point_data = SerializerMethodField()

    def get_date_transfer(self, order_item_point):
        return OrderDateDetailSerializer(order_item_point.date_transfer).data

    def get_trustee_type(self, order_item_point):
        if self.context['is_limited_access']:
            return None
        for item in OrderItemPoint.TRUSTEE_TYPES:
            if item[1].lower() == order_item_point.trustee_content_type.model.lower():
                self.trustee_type = item[0]
                return item[0]
        raise ApiStandartException(dict(error_code=400027))

    def get_trustee_data(self, order_item_point):
        if self.context['is_limited_access'] or self.trustee_type == 0:
            return None
        elif self.trustee_type == 1:
            return TrusteeSerializer(order_item_point.trustee_content_object).data

    def get_point_type(self, order_item_point):
        for item in OrderItemPoint.POINTS_TYPES:
            if item[1].lower() == order_item_point.point_content_type.model.lower():
                self.point_type = item[0]
                return item[0]
        raise ApiStandartException(dict(error_code=400026))

    def get_point_data(self, order_item_point):
        if self.point_type == 0:
            if not order_item_point.point_user or self.context['is_limited_access']:
                return order_item_point.point_content_object.get_limited(
                    self.context['is_limited_access']
                )
            else:
                return PointUserSerializer(order_item_point.point_user).data

    class Meta:
        model = OrderItemPoint
        fields = ('id', 'date_transfer', 'trustee_type', 'trustee_data', 'point_type', 'point_data')


class ShortOrderItemPointDetailSerializer(CustomModelSerializer):
    date_transfer = SerializerMethodField()
    point_type = SerializerMethodField()
    point_data = SerializerMethodField()

    def get_date_transfer(self, order_item_point):
        return OrderDateDetailSerializer(order_item_point.date_transfer).data

    def get_point_type(self, order_item_point):
        for item in OrderItemPoint.POINTS_TYPES:
            if item[1].lower() == order_item_point.point_content_type.model.lower():
                self.point_type = item[0]
                return item[0]
        raise ApiStandartException(dict(error_code=400026))

    def get_point_data(self, order_item_point):
        if self.point_type == 0:
            if not order_item_point.point_user or self.context['is_limited_access']:
                try:
                    return order_item_point.point_content_object.get_limited(
                        self.context['is_limited_access']
                    )
                except AttributeError:
                    return None
            else:
                return PointUserSerializer(order_item_point.point_user).data

    class Meta:
        model = OrderItemPoint
        fields = ('id', 'date_transfer', 'point_type', 'point_data')


class OrderItemSerializer(CustomModelSerializer):
    point_acceptance = OrderItemPointSerializer()
    point_delivery = OrderItemPointSerializer()
    characteristics = CharacteristicCreateSerializer(many=True)

    class Meta:
        model = OrderItem
        fields = ('title', 'description', 'point_acceptance', 'point_delivery', 'characteristics')


class OrderItemDetailSerializer(CustomModelSerializer):
    point_acceptance = SerializerMethodField()
    point_delivery = SerializerMethodField()
    characteristics = SerializerMethodField()
    coord = SerializerMethodField()

    def get_point_acceptance(self, order_item):
        return OrderItemPointDetailSerializer(order_item.point_acceptance,
                                              context=dict(is_limited_access=self.context['is_limited_access'])).data

    def get_point_delivery(self, order_item):
        return OrderItemPointDetailSerializer(order_item.point_delivery,
                                              context=dict(is_limited_access=self.context['is_limited_access'])).data

    def get_characteristics(self, order_item):
        return OrderItemCategoryCargoCharacteristicSerializer(order_item.order_item_characteristics.all(),
                                                              many=True).data

    def get_coord(self, order_item):
        return order_item.get_coord(self.context['request'].user)

    class Meta:
        model = OrderItem
        fields = ('uuid', 'title', 'description', 'status', 'point_acceptance', 'point_delivery',
                  'created_at', 'characteristics', 'coord')


class OrderItemShortSerializer(CustomModelSerializer):
    point_acceptance = SerializerMethodField()
    point_delivery = SerializerMethodField()
    coord = SerializerMethodField()

    def get_point_acceptance(self, order_item):
        return ShortOrderItemPointDetailSerializer(order_item.point_acceptance,
                                                   context=dict(
                                                       is_limited_access=self.context['is_limited_access'])).data

    def get_point_delivery(self, order_item):
        return ShortOrderItemPointDetailSerializer(order_item.point_delivery,
                                                   context=dict(
                                                       is_limited_access=self.context['is_limited_access'])).data

    def get_coord(self, order_item):
        return order_item.get_coord(self.context['request'].user)

    class Meta:
        model = OrderItem
        fields = ('uuid', 'title', 'description', 'status', 'point_acceptance', 'point_delivery',
                  'created_at', 'coord')


class CategoryCargoRelatedField(SlugRelatedField):
    def to_representation(self, obj):
        return SortedCategoryCargoSerializer(obj).data


class CostCargoSerializer(CustomSerializer):
    value = FloatField()
    currency = CurrencyRelatedField(queryset=Currency.objects.all(), slug_field='slug', required=True)

    def validate_value(self, value):
        return value


class OrderSerializer(CustomModelSerializer):
    category_cargo = CategoryCargoRelatedField(queryset=CategoryCargo.objects.all(), slug_field='slug')
    transportation_type = TransportationTypeRelatedField(queryset=TransportationType.objects.all(), slug_field='slug')
    order_items = OrderItemSerializer(many=True)
    payment_info = OrderPaymentInfoSerializer()
    cost_cargo = CostCargoSerializer()

    class Meta:
        model = Order
        fields = ('title', 'description', 'category_cargo', 'transportation_type', 'order_type', 'valid_until',
                  'distance', 'is_published', 'cost_cargo', 'with_items', 'order_items',
                  'payment_info')

    def validate_cost_cargo(self, cost_cargo):
        if cost_cargo < 0:
            raise ApiStandartException(dict(error_code=400078))
        return cost_cargo

    def validate(self, data):
        try:
            data['cat_cargo_tr_type'] = CategoryCargoTransportationType.objects.get(
                category_cargo=data['category_cargo'],
                transportation_type=data['transportation_type']
            )
        except CategoryCargoTransportationType.DoesNotExist:
            raise ApiStandartException(dict(error_code=400031))

        if data['valid_until'] <= pytz.UTC.localize(datetime.datetime.utcnow()):
            raise ApiStandartException(dict(error_code=400083))
        for item in data['order_items']:
            OrderDate.check_transfer_order_item_date(
                item['point_acceptance']['date_transfer'],
                item['point_delivery']['date_transfer'],
                data['valid_until']
            )

        cost_cargo = data.pop('cost_cargo')
        data['cost_cargo'] = cost_cargo['value']
        data['cost_cargo_currency'] = cost_cargo['currency']

        if not data['order_items']:
            raise ApiStandartException(dict(error_code=400044))

        for index, item in enumerate(data['order_items']):
            for index_characteristic, char_item in enumerate(item['characteristics']):
                try:
                    data['order_items'][index]['characteristics'][index_characteristic][
                        'category_cargo_characteristic'] = \
                        CategoryCargoCharacteristic.objects.get(characteristic=char_item['characteristic'],
                                                                category_cargo=data['category_cargo'])
                except CategoryCargoCharacteristic.DoesNotExist:
                    raise ApiStandartException(dict(error_code=400032))

            data['order_items'][index]['point_acceptance']['point_user'] = None
            data['order_items'][index]['point_delivery']['point_user'] = None
            if item['point_acceptance']['point_type'] == 0:
                data['order_items'][index]['point_acceptance']['point_user'] = PointUser.objects.create(
                    **item['point_acceptance']['point_data'])
                data['order_items'][index]['point_acceptance']['obj'] = Address.make_from_coord(
                    item['point_acceptance']['point_data']['latitude'],
                    item['point_acceptance']['point_data']['longitude']
                )

            if item['point_delivery']['point_type'] == 0:
                data['order_items'][index]['point_delivery']['point_user'] = PointUser.objects.create(
                    **item['point_delivery']['point_data'])
                data['order_items'][index]['point_delivery']['obj'] = Address.make_from_coord(
                    item['point_delivery']['point_data']['latitude'],
                    item['point_delivery']['point_data']['longitude']
                )
        return data

    def create(self, validated_data):
        status = 0
        date_published = None
        if validated_data.get('is_published'):
            status = 1
            date_published = datetime.datetime.utcnow()

        transportation_type = validated_data.pop('transportation_type')
        cat_cargo_tr_type_data = validated_data.pop('cat_cargo_tr_type')
        order_items_data = validated_data.pop('order_items')

        category_cargo = validated_data.pop('category_cargo')
        for item in order_items_data:
            CategoryCargoCharacteristic.check_main_characteristics(item['characteristics'], category_cargo)

        payment_info = OrderPaymentInfo.objects.create(**validated_data.pop('payment_info'))

        route = None
        if transportation_type.slug == 'point-to-point':
            point_acceptance = order_items_data[0]['point_acceptance'].get('obj')
            point_delivery = order_items_data[0]['point_delivery'].get('obj')

            if type(point_acceptance).__name__.lower() == 'address':
                start_lat = point_acceptance.address_coord.latitude
                start_lon = point_acceptance.address_coord.longitude

            if type(point_delivery).__name__.lower() == 'address':
                destination_lat = point_delivery.address_coord.latitude
                destination_lon = point_delivery.address_coord.longitude

            route = get_route_g(start_lat, start_lon, destination_lat, destination_lon)
        if 'distance' in route:
            distance = int(route['distance'] / 1000)
        else:
            raise ApiStandartException(dict(error_code=400082))


        order = Order.objects.create(
            uuid=uuid.uuid4(),
            title=validated_data.get('title'),
            description=validated_data.get('description', None),
            customer=self.context['request'].user,
            category_cargo_transportation_type=cat_cargo_tr_type_data,
            order_type=validated_data.get('order_type'),
            valid_until=validated_data.get('valid_until'),
            is_published=validated_data.get('is_published'),
            payment_info=payment_info,
            cost_cargo=validated_data.get('cost_cargo'),
            cost_cargo_currency=validated_data.get('cost_cargo_currency'),
            with_items=validated_data.get('with_items'),
            date_published=date_published,
            status=status,
            distance=distance
        )

        for item in order_items_data:
            characteristics_data = item.pop('characteristics')
            if 'trustee_data' in item['point_acceptance'] and item['point_acceptance']['trustee_data']:
                trustee_acceptance = Trustee.objects.create(**item['point_acceptance']['trustee_data'])
            else:
                trustee_acceptance = self.context['request'].user

            if 'trustee_data' in item['point_delivery'] and item['point_delivery']['trustee_data']:
                trustee_delivery = Trustee.objects.create(**item['point_delivery']['trustee_data'])
            else:
                trustee_delivery = self.context['request'].user
            date_acceptance = OrderDate.objects.create(**item['point_acceptance']['date_transfer'])
            date_delivery = OrderDate.objects.create(**item['point_delivery']['date_transfer'])

            point_acceptance = OrderItemPoint.objects.create(
                trustee_object_id=trustee_acceptance.id,
                trustee_content_type=ContentType.objects.get_for_model(trustee_acceptance),
                point_object_id=item['point_acceptance']['obj'].id,
                point_content_type=ContentType.objects.get_for_model(item['point_acceptance'].get('obj')),
                point_user=item['point_acceptance'].get('point_user'),
                date_transfer=date_acceptance

            )
            point_delivery = OrderItemPoint.objects.create(
                trustee_object_id=trustee_delivery.id,
                trustee_content_type=ContentType.objects.get_for_model(trustee_delivery),
                point_object_id=item['point_delivery']['obj'].id,
                point_content_type=ContentType.objects.get_for_model(item['point_delivery'].get('obj')),
                point_user=item['point_delivery'].get('point_user'),
                date_transfer=date_delivery

            )
            order_item = OrderItem.objects.create(
                uuid=uuid.uuid4(),
                title=item['title'],
                description=item['description'] if 'description' in item else None,
                order=order,
                status=status,
                point_acceptance=point_acceptance,
                point_delivery=point_delivery
            )
            for characteristic in characteristics_data:
                characteristic.pop('characteristic')
                OrderItemCategoryCargoCharacteristic.objects.create(
                    order_item=order_item,
                    **characteristic
                )
        if validated_data.get('is_published'):
            CheckValidOrder.check.apply_async(
                (order.uuid,),
                eta=order.valid_until
            )
        return order


class OrderDetailSerializer(CustomModelSerializer):
    order_items = SerializerMethodField()
    category_cargo = SerializerMethodField()
    transportation_type = SerializerMethodField()
    payment_info = OrderPaymentInfoSerializer()
    date_published = CustomDateTimeField()
    date_completed = CustomDateTimeField()
    valid_until = CustomDateTimeField()
    cost_cargo = SerializerMethodField()

    def get_category_cargo(self, order):
        return CategoryCargoDetailSerializer(order.category_cargo_transportation_type.category_cargo).data

    def get_transportation_type(self, order):
        return TransportationTypeSerializer(order.category_cargo_transportation_type.transportation_type).data

    def get_order_items(self, order):
        is_limited_access = self.context['request'].user.check_order_limited_access(order)
        return OrderItemDetailSerializer(order.order_items.all(), many=True,
                                         context=dict(is_limited_access=is_limited_access, request=self.context['request'])).data

    def get_cost_cargo(self, order):
        return CostCargoSerializer(dict(value=order.cost_cargo, currency=order.cost_cargo_currency)).data

    class Meta:
        model = Order
        fields = ('uuid', 'title', 'description', 'category_cargo', 'transportation_type', 'payment_info', 'order_type',
                  'valid_until', 'distance', 'is_published', 'cost_cargo', 'with_items',
                  'order_items', 'status', 'date_completed', 'date_published')


class OrderWebShortSerializer(CustomModelSerializer):
    order_items = SerializerMethodField()
    category_cargo = SerializerMethodField()
    transportation_type = SerializerMethodField()
    date_published = CustomDateTimeField()
    date_completed = CustomDateTimeField()
    valid_until = CustomDateTimeField()
    address_delivery = SerializerMethodField()
    address_acceptance = SerializerMethodField()
    payment_info = OrderPaymentInfoSerializer()
    order_type = SerializerMethodField()
    status = SerializerMethodField()

    def get_category_cargo(self, order):
        return CategoryCargoShortSerializer(order.category_cargo_transportation_type.category_cargo).data

    def get_transportation_type(self, order):
        return TransportationTypeSerializer(order.category_cargo_transportation_type.transportation_type).data

    def get_order_items(self, order):
        is_limited_access = self.context['request'].user.check_order_limited_access(order)
        return OrderItemShortSerializer(order.order_items.all(), many=True,
                                        context=dict(is_limited_access=is_limited_access, request=self.context['request'])).data

    def get_address_delivery(self, order):
        order_item = order.order_items.first()
        if order_item:
            return order.get_common_address(self.context['request'].user, order_item.point_delivery)
        return None

    def get_address_acceptance(self, order):
        order_item = order.order_items.first()
        if order_item:
            return order.get_common_address(self.context['request'].user, order_item.point_acceptance)
        return None

    def get_order_type(self, order):
        return dict(id=order.order_type, title=order.get_order_type_display())

    def get_status(self, order):
        return dict(id=order.status, title=order.get_status_display())

    class Meta:
        model = Order
        fields = ('uuid', 'title', 'description', 'category_cargo', 'transportation_type', 'order_type',
                  'valid_until', 'distance', 'is_published', 'cost_cargo', 'with_items',
                  'order_items', 'status', 'date_completed', 'date_published', 'address_delivery', 'payment_info',
                  'address_acceptance')


class OrderMobileShortSerializer(CustomModelSerializer):
    category_cargo = SerializerMethodField()
    transportation_type = SerializerMethodField()
    date_published = CustomDateTimeField()
    date_completed = CustomDateTimeField()
    valid_until = CustomDateTimeField()
    address_delivery = SerializerMethodField()
    address_acceptance = SerializerMethodField()
    payment_info = OrderPaymentInfoSerializer()
    order_type = SerializerMethodField()
    status = SerializerMethodField()

    def get_category_cargo(self, order):
        return CategoryCargoShortSerializer(order.category_cargo_transportation_type.category_cargo).data

    def get_transportation_type(self, order):
        return TransportationTypeSerializer(order.category_cargo_transportation_type.transportation_type).data

    def get_address_delivery(self, order):
        order_item = order.order_items.first()
        if order_item:
            return order.get_common_address(self.context['request'].user, order_item.point_delivery)
        return None

    def get_address_acceptance(self, order):
        order_item = order.order_items.first()
        if order_item:
            return order.get_common_address(self.context['request'].user, order_item.point_acceptance)
        return None

    def get_order_type(self, order):
        return dict(id=order.order_type, title=order.get_order_type_display())

    def get_status(self, order):
        return dict(id=order.status, title=order.get_status_display())

    class Meta:
        model = Order
        fields = ('uuid', 'title', 'description', 'category_cargo', 'transportation_type', 'order_type',
                  'valid_until', 'distance', 'status', 'date_completed', 'date_published', 'address_delivery',
                  'payment_info', 'address_acceptance')


class OrderSearchSerializer(CustomSerializer):
    mine = BooleanField(default=False, required=False)
    preview_type = IntegerField()  # preview types - 0 - web, 1 - mobile
    title = CharField(required=False)  # название груза или имя заказчика, или номер заказа
    category = ListField(
        child=SlugRelatedField(queryset=CategoryCargo.objects.all(), slug_field='slug', required=False)
    )
    date_published = ListField(
        required=False,
        child=DateField(required=False)
    )
    date_finished = ListField(
        required=False,
        child=DateField(required=False)
    )
    address_acceptance = CharField(required=False)
    address_delivery = CharField(required=False)

    statuses = ListField(
        required=False,
        child=IntegerField(min_value=0, max_value=7)
    )

    south_west_lat = FloatField(required=False)
    south_west_lng = FloatField(required=False)
    north_east_lat = FloatField(required=False)
    north_east_lng = FloatField(required=False)

    is_actual = BooleanField(default=False, required=False)

    def validate_mine(self, mine):
        if mine:
            if self.context['request'].user.is_customer():
                self.context['queryset'] = self.context['queryset'].filter(customer=self.context['request'].user)
            elif self.context['request'].user.is_transporter():
                self.context['queryset'] = self.context['queryset'].filter(o_bettors__bettor=self.context['request'].user, o_bettors__order_bets__status__in=Bet.get_active_statuses_for_transporter())
        else:
            self.context['queryset'] = self.context['queryset'].exclude(status__in=[0, 4, 5, 2])

    def validate_preview_type(self, preview_type):
        if preview_type in [0, 1]:
            return preview_type
        raise ApiStandartException(dict(error_code=400035))

    def validate_title(self, title):
        if title:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(customer__nickname__icontains=title) |
                Q(order_items__title__icontains=title) |
                Q(title__icontains=title)
            )

    def validate_category(self, category):
        if category:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(category_cargo_transportation_type__category_cargo__parent__in=category) or
                Q(category_cargo_transportation_type_category_cargo__in=category)
            )

    def validate_date_published(self, date_published):
        if len(date_published) > 1:
            date_published = [min(date_published), max(date_published)]
            self.context['queryset'] = self.context['queryset'].filter(
                Q(date_published__range=date_published)
            )
        elif len(date_published) == 1:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(date_published__icontains=date_published)
            )

    def validate_date_finished(self, date_finished):
        if len(date_finished) > 1:
            date_finished = [min(date_finished), max(date_finished)]
            self.context['queryset'] = self.context['queryset'].filter(
                Q(date_completed__range=date_finished)
            )
        elif len(date_finished) == 1:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(date_completed__icontains=date_finished)
            )

    def validate_address_acceptance(self, address_acceptance):
        if address_acceptance:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_acceptance__point_user__address__icontains=address_acceptance)
            )

    def validate_address_delivery(self, address_delivery):
        if address_delivery:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_delivery__point_user__address__icontains=address_delivery)
            )

    def validate_statuses(self, statuses):
        if statuses:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(status__in=statuses)
            )

    def validate_south_west_lat(self, south_west_lat):
        if south_west_lat:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_acceptance__point_user__latitude__gte=south_west_lat)
            )

    def validate_north_east_lat(self, north_east_lat):
        if north_east_lat:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_acceptance__point_user__latitude__lte=north_east_lat)
            )

    def validate_south_west_lng(self, south_west_lng):
        if south_west_lng:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_acceptance__point_user__longitude__gte=south_west_lng)
            )

    def validate_north_east_lng(self, north_east_lng):
        if north_east_lng:
            self.context['queryset'] = self.context['queryset'].filter(
                Q(order_items__point_acceptance__point_user__longitude__lte=north_east_lng)
            )

    def validate_is_actual(self, is_actual, user=None):
        if is_actual:
            # todo добавить фильтрацию - заказ доставлени, но на него не был оставлен отзыв
            if self.context['request'].user.is_customer():
                self.context['queryset'] = self.context['queryset'].filter(status__in=Order.get_customer_actual_statuses())
            elif self.context['request'].user.is_transporter():
                self.context['queryset'] = self.context['queryset'].filter(
                    Q(status__in=Order.get_transporter_actual_statuses()) |
                    Q(status_in=Order.get_transporter_mine_actual_statuses(), o_bettors__bettor=user,
                      o_bettors__order_bets__status=Bet.get_winner_status())
                ).order_by('pk').dispatch()


class BetSerializer(CustomModelSerializer):
    order_bettor = SerializerMethodField()
    vehicle = SerializerMethodField()
    carts = CartShortSerializer(many=True)
    status = SerializerMethodField()
    created_at = CustomDateTimeField()
    expired_at = CustomDateTimeField()

    def get_order_bettor(self, bet):
        return UserSerializer(bet.order_bettor.bettor).data

    def get_vehicle(self, bet):
        if bet.vehicle:
            return VehicleShortSerializer(bet.vehicle, context=dict(request=self.context['request'])).data
        else:
            return None

    def get_status(self, bet):
        return dict(id=bet.status, title=bet.get_status_display())

    class Meta:
        model = Bet
        fields = ('id', 'cost', 'comment', 'order_bettor', 'vehicle', 'carts', 'status', 'created_at', 'expired_at')


class ShortBetSerializer(CustomModelSerializer):
    vehicle = SerializerMethodField()
    carts = CartShortSerializer(many=True)
    status = SerializerMethodField()
    created_at = CustomDateTimeField()
    expired_at = CustomDateTimeField()

    def get_status(self, bet):
        return dict(id=bet.status, title=bet.get_status_display())

    def get_vehicle(self, bet):
        if bet.vehicle:
            return VehicleShortSerializer(bet.vehicle, context=dict(request=self.context['request'])).data
        else:
            return None

    class Meta:
        model = Bet
        fields = ('id', 'cost', 'comment', 'vehicle', 'carts', 'status', 'created_at', 'expired_at')


class BetCreateSerializer(CustomModelSerializer):
    order = PrimaryKeyRelatedField(queryset=Order.objects.filter(status=1))
    vehicle = PrimaryKeyRelatedField(queryset=Vehicle.objects.all(), required=False)
    carts = PrimaryKeyRelatedField(queryset=Cart.objects.all(), many=True, required=False)

    def validate_vehicle(self, vehicle):
        if vehicle.user != self.context['request'].user:
            raise ApiStandartException(dict(error_code=400010))
        return vehicle

    def validate_carts(self, carts):
        for cart in carts:
            if cart.user != self.context['request'].user:
                raise ApiStandartException(dict(error_code=400037))
        return carts

    def validate_order(self, order):
        if order.status != 1:
            raise ApiStandartException(dict(error_code=400039))
        if not order.valid_until or order.valid_until < pytz.UTC.localize(datetime.datetime.utcnow()):
            raise ApiStandartException(dict(error_code=400050))
        return order

    class Meta:
        model = Bet
        fields = ('cost', 'comment', 'vehicle', 'carts', 'order')

    def validate(self, attrs):
        order = attrs.pop('order')
        order_bettor, is_created = OrderBettor.objects.get_or_create(bettor=self.context['request'].user, order=order)
        prev_bets = Bet.objects.filter(order_bettor=order_bettor)
        for item in prev_bets:
            if item.status == 1:
                raise ApiStandartException(dict(error_code=400040))
            elif item.status == 0:
                item.status = 6
                item.save()
        attrs['order_bettor'] = order_bettor
        return attrs

    def create(self, validated_data):
        carts = None
        if 'carts' in validated_data:
            carts = validated_data.pop('carts')
        bet = Bet.objects.create(**validated_data)
        if carts:
            bet.carts.add(*carts)
        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=settings.TRANSPORTER_ADD_BET_LETTER_SUBJECT,
                    template_text=settings.TRANSPORTER_ADD_BET_LETTER_TXT,
                    template_html=settings.TRANSPORTER_ADD_BET_LETTER_HTML,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[bet.order_bettor.order.customer.email],
                    params=dict(
                        order_title=bet.order_bettor.order.title,
                        file_server_url=settings.FILE_SERVER_URL,
                        transporter_nickname=bet.order_bettor.bettor.nickname
                    )
                )
            )
        )
        return bet


class RequestCustomerAcceptBetSerializer(CustomSerializer):
    expired_time = TimeField()


class RequestTransporterAcceptanceBetSerializer(CustomSerializer):
    date_acceptance = DateTimeField()

    def validate_date_acceptance(self, date_acceptance):
        order_item = self.context['order'].order_items.all().first()
        if order_item:
            order_item.point_delivery.date_transfer.check_correctness(date_acceptance, order_item.order.valid_until)
            return date_acceptance
        return None


class RequestListBetSerializer(CustomSerializer):
    order = PrimaryKeyRelatedField(queryset=Order.objects.all())


class ListBetSerializer(CustomSerializer):
    bettor = UserSerializer()
    bets = ShortBetSerializer(many=True)


class RequestResendOrderCertificates(CustomSerializer):
    order = UUIDField(format='hex_verbose')

    def validate_order(self, order):
        order = Order.get_by_uuid(order)
        if order.customer != self.context['request'].user:
            raise ApiStandartException(dict(error_code=400072))
        if not order.check_carried_out_status():
            raise ApiStandartException(dict(error_code=400073))
        return order
