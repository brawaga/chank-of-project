from django.core.urlresolvers import reverse
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.test import APITestCase
from accounts.models import CustomUser


class AccountTests(APITestCase):

    def setUp(self):
        Group.objects.create(name=CustomUser.USERS_GROUPS[0][1])
        Group.objects.create(name=CustomUser.USERS_GROUPS[1][1])
        Group.objects.create(name=CustomUser.USERS_GROUPS[2][1])

    def test_create_account(self):
        """
        Ensure we can create a new account object.
        """
        url = '/api/v1/accounts/registration/'
        data = {
                "phone": "7777777777",
                "password": "aaaAAA111",
                "email": "valid_mail@gmail.com",
                "nickname": "Vasya",
                "user_group": 1
            }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CustomUser.objects.all().count(), 1)
        self.assertEqual(CustomUser.objects.get(email='valid_mail@gmail.com').phone, '7777777777')
