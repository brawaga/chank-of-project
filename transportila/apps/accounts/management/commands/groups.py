# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from ...models import CustomUser


def add():
    for item in CustomUser.USERS_GROUPS:
        Group.objects.get_or_create(name=item[1], id=item[0])

    print u'Added groups!'

