from django.core.management.base import BaseCommand
import users
import sites, groups


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'profiles' app\n")
        groups.add()
        sites.add()
        users.add()

        self.stdout.write("Successfully complete\n")
