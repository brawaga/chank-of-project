
__author__ = 'ustinov'

from django.conf.urls import include, url
from rest_framework.routers import SimpleRouter

from .api import RegistrationView, TokenView, RegistrationConfirmView, ResetPasswordView, ActivatePasswordView, \
    ChangePasswordView, CustomUserViewSet, RevokeTokenView

v1_router = SimpleRouter()
v1_router.register('accounts', CustomUserViewSet)

v1_urlpatterns = [
    url(r'accounts/registration/confirm/(?P<key>[\w-]+)/$', RegistrationConfirmView.as_view(), name='confirm_registration'),
    url(r'accounts/registration/$', RegistrationView.as_view()),
    url(r'accounts/token/$', TokenView.as_view()),
    url(r'accounts/password/reset/$', ResetPasswordView.as_view(), name='reset_password'),
    url(r'accounts/password/activate/(?P<key>[\w-]+)/$', ActivatePasswordView.as_view(), name='activate_password'),
    url(r'accounts/password/change/$', ChangePasswordView.as_view()),
    url(r'', include(v1_router.urls))
]

urlpatterns = [
    url(r'^api/v1/', include(v1_urlpatterns)),
    url(r'^api/oauth/revoke-token/$', RevokeTokenView.as_view(), name="revoke-token"),
]
