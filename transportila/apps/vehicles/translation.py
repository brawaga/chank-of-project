from modeltranslation.translator import register, TranslationOptions

from .models import VehicleType, CartType

__author__ = 'sandan'


@register(VehicleType)
class NewsTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(CartType)
class CartTypeTranslationOptions(TranslationOptions):
    fields = ('title',)

