# coding=utf-8

from rest_framework.serializers import SerializerMethodField, IntegerField, ValidationError, ListField, DictField, \
    SlugRelatedField, RelatedField, PrimaryKeyRelatedField
from django.db.models import Prefetch, Q
from django.utils.translation import ugettext_lazy as _

from .models import Vehicle, VehicleType, VehicleBrand, VehicleModel, Cart, CartType, \
    CartTypeCharacteristic, Characteristic, CartCharacteristic
from auxiliaries.models import Attachment, Color
from support.serializers import CustomSerializer, CustomModelSerializer
from auxiliaries.serializers import AttachmentShortSerializer, ColorSerializer, CharacteristicSerializer
from support.exceptions import ApiStandartException

__author__ = 'sandan'


class VehicleModelSerializer(CustomModelSerializer):
    class Meta:
        model = VehicleModel
        fields = ('id', 'title', 'max_bulk', 'max_capacity')


class RequestVehicleModelListSerializer(CustomSerializer):
    vehicle_brand_id = IntegerField()
    vehicle_type_id = IntegerField()

    def validate_vehicle_brand_id(self, vehicle_brand_id):
        try:
            vehicle_brand = VehicleBrand.objects.get(id=vehicle_brand_id)
        except VehicleBrand.DoesNotExist:
            raise ValidationError(_('Vehicle brand does not exist.'))
        return vehicle_brand

    def validate_vehicle_type_id(self, vehicle_type_id):
        try:
            vehicle_type = VehicleType.objects.get(id=vehicle_type_id)
        except Vehicle.DoesNotExist:
            raise ValidationError(_('Vehicle type does not exist.'))
        return vehicle_type


class VehicleSerializer(CustomModelSerializer):
    carts = ListField()
    photos = ListField()
    vehicle_type = IntegerField()
    vehicle_brand = DictField()
    vehicle_model = DictField()

    class Meta:
        model = Vehicle
        fields = ('color', 'carts', 'photos', 'vehicle_type', 'vehicle_brand', 'reg_number',
                  'vin_code', 'comment', 'manufacture_year', 'bulk', 'capacity', 'vehicle_model')

    def validate_carts(self, carts_ids):
        carts = list()
        for cart_id in carts_ids:
            cart = Cart.get_object(cart_id)
            carts.append(cart)
        return carts

    def validate_photos(self, photos_ids):
        return [Attachment.get_object(photo_id, self.context['request'].user) for photo_id in photos_ids]

    def validate_vehicle_type(self, vehicle_type_id):
        self.vehicle_type = VehicleType.get_object(vehicle_type_id)
        return self.vehicle_type

    def validate_vehicle_brand(self, vehicle_brand_dict):
        if 'id' in vehicle_brand_dict and 'title' in vehicle_brand_dict:
            self.vehicle_brand = VehicleBrand.check_object(vehicle_brand_dict['id'], vehicle_brand_dict['title'],
                                                           self.context['request'].user)
        else:
            raise ValidationError(_('Id and title are require.'))
        return self.vehicle_brand

    def validate_vehicle_model(self, model_dict):
        if 'id' in model_dict and 'title' in model_dict:
            model = VehicleModel.check_object(model_dict['id'], model_dict['title'], self.context['request'].user,
                                              self.vehicle_type, self.vehicle_brand)
        else:
            raise ValidationError(_('Id and title are require.'))
        return model

    def validate_vin_code(self, vin_code):
        if not vin_code and self.vehicle_type.must_registered:
            raise ValidationError(_('Vin code is require.'))
        return vin_code

    def validate_reg_number(self, reg_number):
        if not reg_number and self.vehicle_type.must_registered:
            raise ValidationError(_('Registration number is require.'))
        return reg_number

    def save(self, **kwargs):
        vehicle = Vehicle.objects.create(
            reg_number=self.validated_data.get('reg_number'),
            vin_code=self.validated_data.get('vin_code'),
            color=self.validated_data.get('color'),
            bulk=self.validated_data.get('bulk'),
            capacity=self.validated_data.get('capacity'),
            comment=self.validated_data.get('comment'),
            manufacture_year=self.validated_data.get('manufacture_year'),
            user=self.context['request'].user,
            vehicle_model=self.validated_data.get('vehicle_model')
        )
        for cart in self.validated_data.get('carts'):
            cart.set_vehicle(vehicle)

        for photo in self.validated_data.get('photos'):
            photo.set_content_type(vehicle)
        return vehicle

    def update(self, instance, validated_data):
        instance.reg_number = validated_data.get('reg_number', instance.reg_number)
        instance.vin_code = validated_data.get('vin_code', instance.vin_code)
        instance.color = validated_data.get('color', instance.color)
        instance.bulk = validated_data.get('bulk', instance.bulk)
        instance.capacity = validated_data.get('capacity', instance.capacity)
        instance.comment = validated_data.get('comment', instance.comment)
        instance.manufacture_year = validated_data.get('manufacture_year', instance.manufacture_year)
        instance.vehicle_model = validated_data.get('vehicle_model', instance.vehicle_model)
        instance.save()

        instance.update_photos(validated_data.get('photos'), self.context['request'].user)
        instance.update_carts(validated_data.get('carts'))
        return instance


class VehicleShortSerializer(CustomModelSerializer):
    vehicle_type = SerializerMethodField()
    vehicle_brand = SerializerMethodField()
    photo = SerializerMethodField()
    model_title = SerializerMethodField()

    def get_vehicle_type(self, vehicle):
        return vehicle.vehicle_model.vehicle_type_brand.vehicle_type.title

    def get_vehicle_brand(self, vehicle):
        return vehicle.vehicle_model.vehicle_type_brand.vehicle_brand.title

    def get_photo(self, vehicle):
        attachment = vehicle.get_photos(self.context['request'].user).first()
        return AttachmentShortSerializer(attachment).data if attachment is not None else None

    def get_model_title(self, vehicle):
        return vehicle.vehicle_model.title

    class Meta:
        model = Vehicle
        fields = ('id', 'reg_number', 'vehicle_type', 'photo', 'vehicle_brand', 'model_title')


class VehicleDetailSerializer(CustomModelSerializer):
    model = SerializerMethodField()
    color = SerializerMethodField()
    carts = SerializerMethodField()
    photos = SerializerMethodField()
    vehicle_type = SerializerMethodField()
    vehicle_brand = SerializerMethodField()

    def get_model(self, vehicle):
        return VehicleModelSerializer(vehicle.vehicle_model).data

    def get_color(self, vehicle):
        return ColorSerializer(vehicle.color).data

    def get_carts(self, vehicle):
        return CartShortSerializer(vehicle.vehicle_carts.all(), many=True, context=dict(request=self.context['request'])).data

    def get_photos(self, vehicle):
        return AttachmentShortSerializer(vehicle.get_photos(self.context['request'].user), many=True).data

    def get_vehicle_type(self, vehicle):
        return VehicleTypeSerializer(vehicle.vehicle_model.vehicle_type_brand.vehicle_type).data

    def get_vehicle_brand(self, vehicle):
        return VehicleBrandSerializer(vehicle.vehicle_model.vehicle_type_brand.vehicle_brand).data

    class Meta:
        model = Vehicle
        fields = ('id', 'reg_number', 'vin_code', 'comment', 'manufacture_year', 'bulk', 'capacity', 'model',
                  'color', 'carts', 'photos', 'vehicle_type', 'vehicle_brand')


class VehicleTypeSerializer(CustomModelSerializer):
    class Meta:
        model = VehicleType
        fields = ('id', 'title',)


class VehicleBrandSerializer(CustomModelSerializer):
    class Meta:
        model = VehicleBrand
        fields = ('id', 'title',)


class VehicleBrandListSerializer(CustomSerializer):
    vehicle_type_id = IntegerField()

    def validate_vehicle_type_id(self, vehicle_type_id):
        try:
            vehicle_type = VehicleType.objects.prefetch_related(
                Prefetch(
                    'vehicle_brands', queryset=VehicleBrand.objects.filter(
                        Q(user=None) | Q(user=self.context['request'].user)
                    )
                )
            ).get(id=vehicle_type_id)
        except VehicleType.DoesNotExist:
            raise ValidationError('Vehicle type does not exist.')
        return vehicle_type


class CartSerializer(CustomModelSerializer):
    cart_type = PrimaryKeyRelatedField(queryset=CartType.objects.all())
    vehicle = PrimaryKeyRelatedField(queryset=Vehicle.objects.all(), required=False)
    characteristics = ListField()
    photos = ListField(required=False)

    def __init__(self, **kwargs):
        super(CartSerializer, self).__init__(**kwargs)
        self.cart_type = None

    def validate_cart_type(self, cart_type):
        if cart_type:
            self.cart_type = cart_type
            return self.cart_type
        else:
            raise ApiStandartException(dict(error_code=400012))

    def validate_vehicle(self, vehicle):
        if vehicle and vehicle.user == self.context['request'].user:
            return vehicle
        else:
            raise ApiStandartException(dict(error_code=400013))

    def validate_characteristics(self, characteristics_list):
        if self.cart_type:
            characteristics = list()
            for item in characteristics_list:
                if 'slug' in item and item['slug'] and 'value' in item:
                    characteristic = Characteristic.get_object(item['slug'])
                    cart_type_characteristic = CartTypeCharacteristic.get_object(self.cart_type, characteristic)
                    characteristics.append(dict(cart_type_characteristic=cart_type_characteristic, value=item['value']))
                else:
                    raise ValidationError(_("Slug and value are require."))
            return characteristics

    def validate_photos(self, photos_ids):
        return [Attachment.get_object(photo_id, self.context['request'].user) for photo_id in photos_ids]

    class Meta:
        model = Cart
        fields = ('reg_number', 'vin_code', 'comment', 'cart_type', 'vehicle', 'body_type',
                  'characteristics', 'title', 'bulk', 'capacity', 'photos', 'manufacture_year'
                  )

    def save(self, **kwargs):
        cart = Cart.objects.create(
            reg_number=self.validated_data.get('reg_number', None),
            vin_code=self.validated_data.get('vin_code', None),
            comment=self.validated_data.get('comment', None),
            vehicle=self.validated_data.get('vehicle', None),
            title=self.validated_data.get('title', None),
            bulk=self.validated_data.get('bulk', None),
            capacity=self.validated_data.get('capacity', None),
            body_type=self.validated_data.get('body_type', None),
            manufacture_year=self.validated_data.get('manufacture_year', None),
            user=self.context['request'].user
        )
        photos = self.validated_data.get('photos', None)
        if photos:
            for photo in photos:
                photo.set_content_type(cart)

        cart_characteristics = list()
        for item in self.validated_data.get('characteristics', None):
            cart_characteristics.append(
                CartCharacteristic(
                    cart=cart,
                    cart_type_characteristic=item['cart_type_characteristic'],
                    value=item['value']
                )
            )

        CartCharacteristic.objects.bulk_create(cart_characteristics)
        return cart

    def update(self, instance, validated_data):
        instance.reg_number = self.validated_data.get('reg_number', None)
        instance.vin_code = self.validated_data.get('vin_code', None)
        instance.comment = self.validated_data.get('comment', None)
        instance.vehicle = self.validated_data.get('vehicle', None)
        instance.title = self.validated_data.get('title', None)
        instance.bulk = self.validated_data.get('bulk', None)
        instance.capacity = self.validated_data.get('capacity', None)
        instance.manufacture_year = self.validated_data.get('manufacture_year', None)
        instance.body_type = self.validated_data.get('body_type', None)
        instance.save()

        instance.update_photos(self.validated_data.get('photos'), self.context['request'].user)
        instance.update_characteristics(self.validated_data.get('characteristics'))
        return instance


class CartDetailSerializer(CustomModelSerializer):
    cart_type = SerializerMethodField()
    vehicle = SerializerMethodField()
    characteristics = SerializerMethodField()
    photos = SerializerMethodField()

    def get_cart_type(self, cart):
        return CartTypeSerializer(CartType.get_for_cart(cart)).data

    def get_vehicle(self, cart):
        return VehicleShortSerializer(cart.vehicle,
                                      context=dict(request=self.context['request'])).data if cart.vehicle else None

    def get_characteristics(self, cart):
        return CartCharacteristicSerializer(cart.get_characteristics(), many=True).data

    def get_photos(self, cart):
        return AttachmentShortSerializer(cart.get_photos(self.context['request'].user), many=True).data

    class Meta:
        model = Cart
        fields = ('id', 'reg_number', 'vin_code', 'comment', 'cart_type', 'vehicle',
                  'characteristics', 'title', 'bulk', 'capacity', 'photos', 'manufacture_year', 'body_type',
                  )

    def save(self, **kwargs):
        pass


class CartShortSerializer(CustomModelSerializer):
    cart_type = SerializerMethodField()
    photo = SerializerMethodField()

    def get_cart_type(self, cart):
        return CartTypeShortSerializer(
            CartType.objects.filter(cart_types__cart_type_characteristics__cart=cart).first()
        ).data

    def get_photo(self, cart):
        photo = cart.get_photos(self.context['request'].user).first()
        return AttachmentShortSerializer(photo).data if photo is not None else None

    class Meta:
        model = Cart
        fields = ('id', 'cart_type', 'title', 'photo', 'body_type')


class CartTypeSerializer(CustomModelSerializer):
    characteristics = SerializerMethodField()

    def get_characteristics(self, cart_type):
        return CharacteristicSerializer(cart_type.characteristics.all(), many=True).data

    class Meta:
        model = CartType
        fields = ('id', 'title', 'characteristics')


class CartTypeShortSerializer(CustomModelSerializer):
    class Meta:
        model = CartType
        fields = ('id', 'title')


class CartCharacteristicSerializer(CustomModelSerializer):
    unit_title = SerializerMethodField()
    unit_type = SerializerMethodField()
    title = SerializerMethodField()
    slug = SerializerMethodField()

    def get_unit_title(self, cart_characteristic):
        unit = cart_characteristic.cart_type_characteristic.characteristic.unit
        return unit.title if unit else None

    def get_unit_type(self, cart_characteristic):
        return cart_characteristic.cart_type_characteristic.characteristic.unit_type

    def get_title(self, cart_characteristic):
        return cart_characteristic.cart_type_characteristic.characteristic.title

    def get_slug(self, cart_characteristic):
        return cart_characteristic.cart_type_characteristic.characteristic.slug

    class Meta:
        model = CartCharacteristic
        fields = ('slug', 'title', 'unit_title', 'unit_type', 'value')






