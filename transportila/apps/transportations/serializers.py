# coding=utf-8
from django.db.models import Q
from rest_framework.serializers import SerializerMethodField, ListField, CharField, BooleanField
from support.exceptions import ApiStandartException
from support.serializers import CustomSerializer, CustomModelSerializer
from .models import Transportation, TransportationCargoItem, TransferPointTrip, \
    TransportationCargoItemCategoryCargoCharacteristic, TransportationTransferPoint, TransportationSchedule
from django.conf import settings
from payments.serializers import CurrencySerializer
from auxiliaries.serializers import CharacteristicSerializer, CharacteristicCreateSerializer
from auxiliaries.models import Characteristic, Attachment
from orders.models import Bet
from queue.send_email import SendEmail

__author__ = 'sandan'


class TransportationTransferPointDetailSerializer(CustomModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()
    address = SerializerMethodField()
    cargo_items = SerializerMethodField()
    parent = SerializerMethodField()
    child = SerializerMethodField()
    arrival_at = SerializerMethodField()

    def get_latitude(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.address_coord.latitude

    def get_longitude(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.address_coord.longitude

    def get_address(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.title

    def get_cargo_items(self, transportation_transfer_point):
        # actions: 0 - acceptance, 1 - delivery
        cargo_items = list(self.context['transportation'].transportation_cargo_items.all())
        structured_cargo_items = list()
        for item in cargo_items:
            if item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint' \
                    and item.acceptance_point_content_object.transfer_point == transportation_transfer_point.transfer_point:
                structured_cargo_items.append(
                    dict(
                        uuid=item.pk,
                        action=0,
                        trustee=item.acceptance_point_content_object.get_short_trustee_data()
                    )
                )
                continue

            if item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint' \
                    and item.delivery_point_content_object.transfer_point == transportation_transfer_point.transfer_point:
                structured_cargo_items.append(
                    dict(
                        uuid=item.pk,
                        action=1,
                        trustee=item.delivery_point_content_object.get_short_trustee_data()
                    )
                )
        return structured_cargo_items

    def get_parent(self, transportation_transfer_point):
        if transportation_transfer_point.parent:
            return transportation_transfer_point.parent.pk
        return None

    def get_child(self, transportation_transfer_point):
        if transportation_transfer_point.child:
            return transportation_transfer_point.child.pk
        return None

    def get_arrival_at(self, transportation_transfer_point):
        transportation_schedule = transportation_transfer_point.transportation_transfer_point_schedules.first()
        return transportation_schedule.arrival_at.strftime("%Y-%m-%dT%H:%M")

    class Meta:
        model = TransportationTransferPoint
        fields = ('uuid', 'latitude', 'longitude', 'address', 'cargo_items', 'parent', 'child', 'route', 'distance',
                  'is_completed', 'arrival_at')


class TransportationTransferPointShortSerializer(CustomModelSerializer):
    latitude = SerializerMethodField()
    longitude = SerializerMethodField()
    address = SerializerMethodField()
    arrival_at = SerializerMethodField()

    def get_latitude(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.address_coord.latitude

    def get_longitude(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.address_coord.longitude

    def get_address(self, transportation_transfer_point):
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            return transportation_transfer_point.transfer_point.point_content_object.title

    def get_arrival_at(self, transportation_transfer_point):
        transportation_schedule = transportation_transfer_point.transportation_transfer_point_schedules.first()
        return transportation_schedule.arrival_at.strftime("%Y-%m-%dT%H:%M")

    class Meta:
        model = TransportationTransferPoint
        fields = ('uuid', 'latitude', 'longitude', 'address', 'arrival_at')


class TransportationCargoItemCategoryCargoCharacteristicSerializer(CustomModelSerializer):
    characteristic = SerializerMethodField()
    position = SerializerMethodField()

    def get_characteristic(self, item):
        return CharacteristicSerializer(item.category_cargo_characteristic.characteristic).data

    def get_position(self, item):
        return item.category_cargo_characteristic.position

    class Meta:
        model = TransportationCargoItemCategoryCargoCharacteristic
        fields = ('stated_value', 'transporter_value', 'characteristic', 'position')


class TransportationCargoItemDetailSerializer(CustomModelSerializer):
    title = SerializerMethodField()
    description = SerializerMethodField()
    status = SerializerMethodField()
    characteristics = SerializerMethodField()

    def get_characteristics(self, transportation_cargo_item):
        transportation_cargo_items_characteristics = list(
            transportation_cargo_item.transportation_cargo_item_characteristics.all()
        )
        return TransportationCargoItemCategoryCargoCharacteristicSerializer(
            sorted(
                transportation_cargo_items_characteristics,
                key=lambda item: item.category_cargo_characteristic.position
            ),
            many=True
        ).data

    def get_title(self, transportation_cargo_item):
        return transportation_cargo_item.cargo_item_content_object.title

    def get_description(self, transportation_cargo_item):
        return transportation_cargo_item.cargo_item_content_object.description

    def get_status(self, transportation_cargo_item):
        return dict(
            id=transportation_cargo_item.status,
            title=transportation_cargo_item.get_status_display()
        )

    class Meta:
        model = TransportationCargoItem
        fields = ('uuid', 'title', 'description', 'status', 'characteristics')


class TransportationCargoItemTransferSerializer(CustomModelSerializer):
    transportation_uuid = SerializerMethodField()
    title = SerializerMethodField()
    description = SerializerMethodField()
    status = SerializerMethodField()
    characteristics = SerializerMethodField()
    trustee = SerializerMethodField()
    point = SerializerMethodField()
    is_completed = SerializerMethodField()

    def get_transportation_uuid(self, transportation_cargo_item):
        return transportation_cargo_item.transportation.pk

    def get_characteristics(self, transportation_cargo_item):
        transportation_cargo_items_characteristics = list(
            transportation_cargo_item.transportation_cargo_item_characteristics.all()
        )
        return TransportationCargoItemCategoryCargoCharacteristicSerializer(
            sorted(
                transportation_cargo_items_characteristics,
                key=lambda item: item.category_cargo_characteristic.position
            ),
            many=True
        ).data

    def get_title(self, transportation_cargo_item):
        return transportation_cargo_item.cargo_item_content_object.title

    def get_description(self, transportation_cargo_item):
        return transportation_cargo_item.cargo_item_content_object.description

    def get_status(self, transportation_cargo_item):
        return dict(
            id=transportation_cargo_item.status,
            title=transportation_cargo_item.get_status_display()
        )

    def get_trustee(self, transportation_cargo_item):
        if self.context['transportation_point_type'] == 0:
            if transportation_cargo_item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                return transportation_cargo_item.acceptance_point_content_object.get_full_trustee_data(
                    transportation_cargo_item.transportation
                )
        elif self.context['transportation_point_type'] == 1:
            if transportation_cargo_item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                return transportation_cargo_item.delivery_point_content_object.get_full_trustee_data(
                    transportation_cargo_item.transportation
                )
        return None

    def get_point(self, transportation_cargo_item):
        if self.context['transportation_point_type'] == 0:
            if transportation_cargo_item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                return TransportationTransferPointShortSerializer(
                    TransportationTransferPoint.objects.get(
                        transportation=transportation_cargo_item.transportation,
                        transfer_point=transportation_cargo_item.acceptance_point_content_object.transfer_point
                    )
                ).data
        elif self.context['transportation_point_type'] == 1:
            if transportation_cargo_item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                return TransportationTransferPointShortSerializer(
                    TransportationTransferPoint.objects.get(
                        transportation=transportation_cargo_item.transportation,
                        transfer_point=transportation_cargo_item.delivery_point_content_object.transfer_point
                    )
                ).data

    def get_is_completed(self, transportation_cargo_item):
        if self.context['transportation_point_type'] == 0:
            if transportation_cargo_item.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                return transportation_cargo_item.acceptance_point_content_object.is_completed
        elif self.context['transportation_point_type'] == 1:
            if transportation_cargo_item.delivery_point_content_type.model.lower() == 'trusteetransportationpoint':
                return transportation_cargo_item.delivery_point_content_object.is_completed
        return None

    class Meta:
        model = TransportationCargoItem
        fields = ('uuid', 'transportation_uuid', 'title', 'description', 'status', 'characteristics', 'trustee',
                  'point', 'is_completed')


class TransportationDetailSerializer(CustomModelSerializer):
    title = SerializerMethodField()
    status = SerializerMethodField()
    cost = SerializerMethodField()
    transfer_points = SerializerMethodField()
    cargo_items = SerializerMethodField()
    characteristics = SerializerMethodField()
    trustees = SerializerMethodField()

    def get_title(self, transportation):
        if transportation.order_content_type.model.lower() == 'order':
            return transportation.order_content_object.title
        return None

    def get_status(self, transportation):
        return dict(
            id=transportation.status,
            title=transportation.get_status_display()
        )

    def get_cost(self, transportation):
        if transportation.order_content_type.model.lower() == 'order':
            winner_bet = Bet.objects.filter(
                order_bettor__order__uuid=transportation.order_content_object.uuid,
                status=2
            ).first()
            return dict(
                value=winner_bet.cost if winner_bet else None,
                currency=CurrencySerializer(transportation.order_content_object.payment_info.currency).data
            )
        return None

    def get_transfer_points(self, transportation):
        return TransportationTransferPointDetailSerializer(
            list(transportation.transportation_transfer_points.all()),
            many=True, context=dict(transportation=transportation)
        ).data

    def get_cargo_items(self, transportation):
        cargo_items = list(transportation.transportation_cargo_items.all())
        return TransportationCargoItemDetailSerializer(cargo_items, many=True).data

    def get_characteristics(self, transportation):
        return CharacteristicSerializer(
            Characteristic.objects.filter(
                category_cargo_characteristics__characteristic_transportations_cargoes_items__transportation_cargo_item__transportation=transportation
            ).order_by('slug').distinct('slug'),
            many=True
        ).data

    def get_trustees(self, transportation):
        points = list()
        for item in list(transportation.transportation_cargo_items.all()):
            points.extend([item.acceptance_point_content_object, item.delivery_point_content_object])

        trustees = list()
        added_trustees = list()
        for item in points:
            if item.__class__.__name__.lower() == 'trusteetransportationpoint':
                is_added = False
                for subitem in added_trustees:
                    if subitem['trustee_id'] == item.trustee_object_id and subitem[
                        'trustee_type'] == item.trustee_content_type:
                        is_added = True
                if not is_added:
                    trustees.append(item.get_full_trustee_data(transportation))
                    added_trustees.append(
                        dict(trustee_id=item.trustee_object_id, trustee_type=item.trustee_content_type))
        return trustees

    class Meta:
        model = Transportation
        fields = ('uuid', 'title', 'status', 'cost', 'transfer_points', 'cargo_items', 'characteristics', 'trustees')


class TransportationListSerializer(CustomModelSerializer):
    title = SerializerMethodField()
    status = SerializerMethodField()
    cost = SerializerMethodField()
    transfer_point = SerializerMethodField()

    def get_title(self, transportation):
        if transportation.order_content_type.model.lower() == 'order':
            try:
                return transportation.order_content_object.title
            except AttributeError:
                return None
        return None

    def get_status(self, transportation):
        return dict(
            id=transportation.status,
            title=transportation.get_status_display()
        )

    def get_cost(self, transportation):
        if transportation.order_content_type.model.lower() == 'order':
            try:
                winner_bet = Bet.objects.filter(
                    order_bettor__order=transportation.order_content_object,
                    status=2
                ).first()
                return dict(
                    value=winner_bet.cost if winner_bet else None,
                    currency=CurrencySerializer(transportation.order_content_object.payment_info.currency).data
                )
            except AttributeError:
                return None
        return None

    def get_transfer_point(self, transportation):
        try:
            transportation_transfer_point = TransportationTransferPoint.objects.get(
                Q(is_completed=False) & Q(Q(parent__is_completed=True) | Q(parent=None)) &
                Q(transportation=transportation) & Q(Q(child=None) | Q(child__is_completed=False))
            )
        except TransportationTransferPoint.DoesNotExist:
            return None
        transportation_schedule = TransportationSchedule.objects.filter(
            transportation_transfer_point=transportation_transfer_point).first()
        latitude = None
        longitude = None
        address = None
        if transportation_transfer_point.transfer_point.point_content_type.model.lower() == 'address':
            try:
                latitude = transportation_transfer_point.transfer_point.point_content_object.address_coord.latitude
                longitude = transportation_transfer_point.transfer_point.point_content_object.address_coord.longitude
                address = transportation_transfer_point.transfer_point.point_content_object.title
            except AttributeError:
                return None
        return dict(
            uuid=transportation_transfer_point.pk,
            latitude=latitude,
            longitude=longitude,
            address=address,
            arrival_at=transportation_schedule.arrival_at.strftime(
                "%Y-%m-%dT%H:%M") if transportation_schedule else None,
            has_next=True if transportation_transfer_point.child else False
        )

    class Meta:
        model = Transportation
        fields = ('uuid', 'title', 'status', 'cost', 'transfer_point')


class RequestCargoTransferSerializer(CustomSerializer):
    characteristics = CharacteristicCreateSerializer(required=False, many=True)
    photos = ListField(required=False)
    comment = CharField(max_length=2048, required=False)

    def validate_photos(self, photos_ids):
        try:
            return [Attachment.get_object(photo_id, self.context['request'].user) for photo_id in photos_ids]
        except Attachment.DoesNotExist:
            raise ApiStandartException(dict(error_code=400008))

    def update(self, instance, validated_data):
        instance.transfer_update_statuses(validated_data.get('transportation_point_type'))
        customer_email = None
        transporter_nickname = None
        cargo_title = None
        if instance.cargo_item_content_type.model.lower() == 'orderitem':
            customer_email = instance.cargo_item_content_object.order.customer.email
            cargo_title = instance.cargo_item_content_object.title
        if instance.transportation.transporter_trip.transporter_content_type.model.lower() == 'customuser':
            transporter_nickname = instance.transportation.transporter_trip.transporter_content_object.nickname
        elif instance.transportation.transporter_trip.transporter_content_type.model.lower() == 'vehicle':
            transporter_nickname = instance.transportation.transporter_trip.transporter_content_object.user.nickname

        if validated_data.get('transportation_point_type') == 0:
            if instance.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                instance.acceptance_point_content_object.set_transporter_comment(validated_data.get('comment'))
                instance.acceptance_point_content_object.update_photos(validated_data.get('photos'),
                                                                       self.context['request'].user)
            instance.set_transporter_characteristics(validated_data.get('characteristics'))
            subject_template = settings.TRANSPORTER_ACCEPTANCE_CARGO_LETTER_SUBJECT
            template_text = settings.TRANSPORTER_ACCEPTANCE_CARGO_LETTER_TXT
            template_html = settings.TRANSPORTER_ACCEPTANCE_CARGO_LETTER_HTML
        elif validated_data.get('transportation_point_type') == 1:
            if instance.acceptance_point_content_type.model.lower() == 'trusteetransportationpoint':
                instance.acceptance_point_content_object.set_transporter_comment(validated_data.get('comment'))
                instance.acceptance_point_content_object.update_photos(validated_data.get('photos'),
                                                                       self.context['request'].user)
            subject_template = settings.TRANSPORTER_DELIVERY_CARGO_LETTER_SUBJECT
            template_text = settings.TRANSPORTER_DELIVERY_CARGO_LETTER_TXT
            template_html = settings.TRANSPORTER_DELIVERY_CARGO_LETTER_HTML

        SendEmail.send.delay(
            dict(
                data=dict(
                    subject_template=subject_template,
                    template_text=template_text,
                    template_html=template_html,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    emails_to=[customer_email],
                    params=dict(
                        transporter_nickname=transporter_nickname,
                        cargo_title=cargo_title,
                        comment=validated_data.get('comment', None),
                        photos=validated_data.get('photos', None),
                        file_server_url=settings.FILE_SERVER_URL,
                        characteristics=validated_data.get('characteristics', None)
                    )
                )
            )
        )


class TransportationSerchSerializer(CustomSerializer):
    is_actual = BooleanField(default=False, required=False)

    def validate_is_actual(self, is_actual):
        if is_actual:
            self.context['queryset'] = self.context['queryset'].filter(status__in=Transportation.get_actual_statuses())
