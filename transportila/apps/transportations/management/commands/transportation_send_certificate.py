from django.core.management.base import BaseCommand
from transportations.models import Transportation
from queue.send_certificate import SendingTransportationCertificates

class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'profiles' app\n")

        SendingTransportationCertificates.send.delay('fc626900-cd7f-4704-829a-de9053f7af92')
        # transportation = Transportation.objects.get(pk='c65032a9-3ec4-45aa-9cea-448a5554cd45')
        # transportation.generate_certificates()
        self.stdout.write('fc626900-cd7f-4704-829a-de9053f7af92\n')
        self.stdout.write("Successfully complete\n")
