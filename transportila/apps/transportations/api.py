from uuid import UUID
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from support.exceptions import ApiStandartException
from rest_framework.response import Response
from django.contrib.contenttypes.models import ContentType
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope

from .models import Transportation, TransportationCargoItem, TrusteeTransportationPoint
from .serializers import TransportationDetailSerializer, TransportationListSerializer, \
    TransportationCargoItemTransferSerializer, RequestCargoTransferSerializer, TransportationSerchSerializer
from auxiliaries.permissions import TransportationPermissions
from vehicles.models import Vehicle

__author__ = 'sandan'


class TransportationPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class TransportationViewSet(ModelViewSet):
    model = Transportation
    serializer_class = TransportationDetailSerializer
    queryset = Transportation.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope, TransportationPermissions)
    pagination_class = TransportationPagination

    def get_queryset(self):
        vehicles_ids = [item['id'] for item in Vehicle.objects.filter(user=self.request.user).values('id')]
        return super(TransportationViewSet, self).get_queryset().filter(
            Q(
                transporter_trip__transporter_object_id=self.request.user.pk,
                transporter_trip__transporter_content_type=ContentType.objects.get_for_model(self.request.user)
            ) | Q(
                transporter_trip__transporter_object_id__in=vehicles_ids,
                transporter_trip__transporter_content_type=ContentType.objects.get_for_model(Vehicle))
        ).order_by('-created_at')

    def get_object(self):
        try:
            transportation_uuid = UUID(self.kwargs['pk'])
        except ValueError:
            raise ApiStandartException(dict(error_code=400065))

        obj = self.model.get_by_pk(transportation_uuid)
        obj.transporter_trip.check_state(self.request.user)
        return obj

    def retrieve(self, request, *args, **kwargs):
        return Response(self.serializer_class(self.get_object()).data)

    def list(self, request, *args, **kwargs):
        serializer = TransportationSerchSerializer(data=request.query_params, context=dict(queryset=self.get_queryset()))
        paginator = self.pagination_class()
        if 'status' in request.query_params and request.query_params['status'] != '':
            status_list = request.query_params['status'].split(',')
            queryset = serializer.context['queryset'].filter(status__in=status_list)
        else:
            queryset = serializer.context['queryset']
        result_pages = paginator.paginate_queryset(queryset, request)
        return paginator.get_paginated_response(
            TransportationListSerializer(result_pages, many=True).data
        )


class ScanTransportationCargoItemViewSet(ModelViewSet):
    model = TransportationCargoItem
    serializer_class = TransportationCargoItemTransferSerializer
    queryset = TransportationCargoItem.objects.all()
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get_object(self):
        # transportation_point types: 0 - acceptance, 1 - delivery
        # transfer with only TrusteeTransportationPoint
        try:
            cargo_item_uuid = UUID(self.kwargs['pk'])
        except ValueError:
            raise ApiStandartException(dict(error_code=400065))

        try:
            trustee_transportation_point = TrusteeTransportationPoint.objects.get(pk=cargo_item_uuid)
        except TrusteeTransportationPoint.DoesNotExist:
            raise ApiStandartException(dict(error_code=400055))

        try:
            obj = self.model.objects.get(
                Q(acceptance_point_object_id=trustee_transportation_point.pk,
                  acceptance_point_content_type=ContentType.objects.get_for_model(trustee_transportation_point)) |
                Q(delivery_point_object_id=trustee_transportation_point.pk,
                  delivery_point_content_type=ContentType.objects.get_for_model(trustee_transportation_point))
            )
        except self.model.DoesNotExist:
            raise ApiStandartException(dict(error_code=400055))

        obj.check_permissions(self.request.user)

        transportation_point_type = None
        if obj.acceptance_point_content_object == trustee_transportation_point:
            transportation_point_type = 0
        elif obj.delivery_point_content_object == trustee_transportation_point:
            if not obj.acceptance_point_content_object.is_completed:
                raise ApiStandartException(dict(error_code=400062))
            transportation_point_type = 1

        return obj, transportation_point_type

    def retrieve(self, request, *args, **kwargs):
        obj, transportation_point_type = self.get_object()
        obj.scan_update_statuses(transportation_point_type)
        return Response(self.serializer_class(obj, context=dict(transportation_point_type=transportation_point_type)).data)

    def update(self, request, *args, **kwargs):
        obj, transportation_point_type = self.get_object()
        obj.check_transfer_state(transportation_point_type)
        serializer = RequestCargoTransferSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        serializer.validated_data['transportation_point_type'] = transportation_point_type
        serializer.update(obj, serializer.validated_data)
        return Response()
