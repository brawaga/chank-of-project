from django.contrib.admin import register, ModelAdmin

from .models import TransferPoint, Trip, TransporterTrip, Transportation, TrusteeTransportationPoint, \
    TruckingCompanyTransportationPoint, TransportationCargoItem, TransportationCargoItemCategoryCargoCharacteristic, \
    TransferPointTrip, ScheduleTrip, Schedule, TransportationTransferPoint, TransportationSchedule


@register(Schedule)
class AdminSchedule(ModelAdmin):
    list_display = ['id', 'title']


@register(ScheduleTrip)
class AdminScheduleTrip(ModelAdmin):
    list_display = ['id', 'arrival_at', 'depart_at']


@register(TransportationSchedule)
class AdminTransportationSchedule(ModelAdmin):
    list_display = ['id', 'depart_at', 'arrival_at', 'version', 'type_check']


@register(TransferPoint)
class AdminTripPoint(ModelAdmin):
    list_display = ['id', 'comment', 'created_at']


@register(TransferPointTrip)
class AdminTransferPointTrip(ModelAdmin):
    list_display = ['id', 'parent', 'child', 'distance']


@register(Trip)
class AdminTrip(ModelAdmin):
    list_display = ['id', 'number', 'created_at']


@register(TransporterTrip)
class AdminTransporterTrip(ModelAdmin):
    list_display = ['id', 'trip']


@register(Transportation)
class AdminTransportation(ModelAdmin):
    list_display = ['uuid', 'created_at']


@register(TransportationTransferPoint)
class AdminTransportationTransferPoint(ModelAdmin):
    list_display = ['uuid', 'distance', 'transportation', 'transfer_point', 'parent', 'child']


@register(TrusteeTransportationPoint)
class AdminTrusteeTransportationPoint(ModelAdmin):
    list_display = ['uuid', 'is_completed', 'completed_at', 'created_at']


@register(TruckingCompanyTransportationPoint)
class AdminTruckingCompanyTransportationPoint(ModelAdmin):
    list_display = ['id', 'track_number', 'company', 'is_completed', 'completed_at', 'created_at']


@register(TransportationCargoItem)
class AdminTransportationOrderItem(ModelAdmin):
    list_display = ['uuid', 'status', 'parent', 'child', 'transportation',
                    'created_at']


@register(TransportationCargoItemCategoryCargoCharacteristic)
class AdminTransporterTripOrderItemCategoryCargoCharacteristic(ModelAdmin):
    pass
