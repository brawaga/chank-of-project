# coding=utf-8
__author__ = 'vol'
from django.conf import settings
from django.db.models import Model, CharField, ForeignKey, DateField, IntegerField, BooleanField, URLField
from django.utils.translation import ugettext_lazy as _

from places.models import Country, Address
from repositories.paypal import PayPal
try:
    User = settings.AUTH_USER_MODEL
except ImportError:
    from django.contrib.auth.models import User


class BasicInfo(Model):
    first_name = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'first name'))
    last_name = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'last name'))
    patronymic = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'patronymic'))
    birth_date = DateField(null=True, blank=True, verbose_name=_(u'birth date'))
    user_address = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'user address'))
    address = ForeignKey(Address, blank=True, null=True, related_name='address_basic_info', verbose_name=_(u'address'))
    phone = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'phone'))
    created_at = DateField(auto_now=True, null=False, verbose_name=_(u'created at'))
    version = IntegerField(default=0, null=False, verbose_name=_(u'version'))
    user = ForeignKey(User, related_name='basic_info_profiles', verbose_name=_(u'user'))

    class Meta:
        verbose_name = _(u'basic info')
        verbose_name_plural = _(u'basic info')
        ordering = ['-version']

    def __unicode__(self):
        return u'basic info: {0} {1}'.format(self.first_name, self.last_name)

    @classmethod
    def get_by_user(cls, user, is_versioned=False):
        info = cls.objects.filter(user=user)
        if is_versioned:
            return info
        else:
            return info.first()

    def get_full_name(self):
        return u'{0} {1} {2}'.format(self.last_name, self.first_name, self.patronymic)


class BillingInfo(Model):
    name = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'name'))
    country = ForeignKey(Country, blank=True, null=True, verbose_name=_(u'country'))
    address = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'address'))
    email = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'email'))
    phone = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'phone'))
    zip = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'zip'))
    confirmed = BooleanField(null=False, default=False, verbose_name=_(u'confirmed'))
    created_at = DateField(auto_now=True, null=False, verbose_name=_(u'created at'))
    version = IntegerField(default=0, null=False, verbose_name=_(u'version'))
    user = ForeignKey(User, verbose_name=_(u'user'))

    class Meta:
        verbose_name = _(u'billing info')
        verbose_name_plural = _(u'billing info')
        ordering = ['-version']

    def __unicode__(self):
        return u'billing info: {0}'.format(self.name)

    @classmethod
    def check_billing(cls, code, user):
        paypal_user = PayPal().get_user_info(code)
        billing = cls.objects.filter(user=user).order_by('-version')
        if billing and billing == paypal_user:
            return True
        else:
            return False

    @classmethod
    def get_by_user(cls, user, is_versioned=False):
        info = cls.objects.filter(user=user)
        if is_versioned:
            return info
        else:
            return info.first()


class DocumentInfo(Model):
    passport_id = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'passport id'))
    iin = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'iin'))
    license_id = CharField(max_length=250, null=True, blank=False, verbose_name=_(u'license id'))
    created_at = DateField(auto_now=True, null=False, verbose_name=_(u'created at'))
    version = IntegerField(default=0, null=False, verbose_name=_(u'version'))
    user = ForeignKey(User, related_name='user_document_info',  verbose_name=_(u'user'))

    class Meta:
        verbose_name = _(u'document info')
        verbose_name_plural = _(u'document info')
        ordering = ['-version']

    def __unicode__(self):
        return u'document info: {0}'.format(self.passport_id)

    @classmethod
    def get_by_user(cls, user, is_versioned=False):
        info = cls.objects.filter(user=user)
        if is_versioned:
            return info
        else:
            return info.first()
