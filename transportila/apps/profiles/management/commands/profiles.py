# -*- coding: utf-8 -*-
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

from ...models import BasicInfo, BillingInfo, DocumentInfo
from places.models import Address


def add():
    user = User.objects.get(id=1)
    create_basic_info(user)
    create_billing_info(user)
    create_document_info(user)


def create_basic_info(user):
    BasicInfo.objects.get_or_create(user=user, defaults=dict(
        first_name=u'Ivan',
        last_name=u'Ivanov',
        patronymic=u'Ivanovich',
        user_address=u'Country, City, Street, 15',
        address=Address.objects.get(pk=1),
        phone=u'+77777777777',
    ))


def create_billing_info(user):
    BillingInfo.objects.get_or_create(user=user, defaults=dict(
        first_name=u'Ivan',
        last_name=u'Ivanov',
        patronymic=u'Ivanovich',
        country_id=1,
        city=u'Almaty',
        address=u'Lenina, 33',
        email=u'fake@fake.com',
        phone=u'234234234234',
        zip=u'2345555',
        confirmed=True,

    ))


def create_document_info(user):
    DocumentInfo.objects.get_or_create(user=user, defaults=dict(
        passport_id=u'54354354354',
        iin=u'5435345',
        license_id=u'345435',
    ))

