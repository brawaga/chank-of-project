# coding=utf-8
__author__ = 'vol'
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView

from profiles.serializers import BasicInfoSerializer, BillingInfoSerializer, DocumentInfoSerializer, ProfileSerializer, \
    UpdateProfileSerializer
from .models import BasicInfo, BillingInfo, DocumentInfo
from support.exceptions import ApiStandartException
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User


class BasicInfoViewSet(ModelViewSet):
    model = BasicInfo
    queryset = BasicInfo.objects.all()
    serializer_class = BasicInfoSerializer
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def create(self, request, *args, **kwargs):
        # todo добавить сущность для связи пользователя информации из профиля, чтобы убрать связь к пользователю
        # напрямую. Это является сильной связью и при изменении данных о заказчике они отразятся на всех заказах.
        from orders.models import Order
        if Order.objects.filter(customer=request.user, status__in=[1, 4, 6]):
            raise ApiStandartException(dict(error_code=400068))
        serializer = self.serializer_class(data=request.data, context=dict(request=request))
        serializer.is_valid()
        basic_info = serializer.save()
        return Response(self.serializer_class(basic_info).data, status=201)

    def list(self, request, *args, **kwargs):
        is_versioned = False
        many = False
        if 'versioned' in request.query_params:
            is_versioned = True
            many = True
        basic_info = self.model.get_by_user(request.user, is_versioned=is_versioned)
        if basic_info:
            return Response(BasicInfoSerializer(basic_info, many=many).data)
        else:
            raise ApiStandartException(dict(error_code=400015))


class BillingInfoViewSet(ModelViewSet):
    model = BillingInfo
    queryset = BillingInfo.objects.all()
    serializer_class = BillingInfoSerializer
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context=dict(request=request))
        serializer.is_valid()
        billing_info = serializer.save()
        return Response(self.serializer_class(billing_info).data, status=201)

    def list(self, request, *args, **kwargs):
        is_versioned = False
        many = False
        if 'versioned' in request.query_params:
            is_versioned = True
            many = True
        if 'token' in request.query_params:
            self.model.check_billing(request.query_params['token'], request.user)
            return Response(dict())

        billing_info = self.model.get_by_user(request.user.id, is_versioned=is_versioned)
        if billing_info:
            return Response(self.serializer_class(billing_info, many=many).data)
        else:
            raise ApiStandartException(dict(error_code=400016))


class DocumentInfoViewSet(ModelViewSet):
    model = DocumentInfo
    queryset = DocumentInfo.objects.all()
    serializer_class = DocumentInfoSerializer
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context=dict(request=request))
        serializer.is_valid()
        document_info = serializer.save()
        return Response(self.serializer_class(document_info).data, status=201)

    def list(self, request, *args, **kwargs):
        is_versioned = False
        many = False
        if 'versioned' in request.query_params:
            is_versioned = True
            many = True
        document_info = self.model.get_by_user(request.user, is_versioned=is_versioned)
        if document_info:
            return Response(DocumentInfoSerializer(document_info, many=many).data)
        else:
            raise ApiStandartException(dict(error_code=400017))


class ProfileView(APIView):
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsAuthenticated, TokenHasReadWriteScope)

    def get(self, request):
        return Response(self.serializer_class(request.user).data)

    def put(self, request):
        serializer = UpdateProfileSerializer(data=request.data, context=dict(request=request))
        serializer.is_valid()
        instance = serializer.update(request.user, serializer.validated_data)
        return Response(self.serializer_class(instance).data)
