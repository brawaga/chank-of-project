# coding=utf-8
__author__ = 'vol'
from django.conf.urls import url, include
from rest_framework import routers

from profiles.api import BillingInfoViewSet, BasicInfoViewSet, DocumentInfoViewSet, ProfileView


v1_router = routers.SimpleRouter()
v1_router.register('billing', BillingInfoViewSet)
v1_router.register('basic', BasicInfoViewSet)
v1_router.register('document', DocumentInfoViewSet)


urlpatterns = [
    url(r'^api/v1/profile/', include(v1_router.urls)),
    url(r'^api/v1/profile/$', ProfileView.as_view()),
]

