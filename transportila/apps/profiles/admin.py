from django.contrib import admin
from django.contrib.admin.decorators import register

from .models import BillingInfo, BasicInfo, DocumentInfo

__author__ = 'sandan'


@register(BillingInfo)
class AdminBillingInfo(admin.ModelAdmin):
    list_display = ['name']


@register(BasicInfo)
class AdminBasicInfo(admin.ModelAdmin):
    list_display = ['first_name', 'last_name']


@register(DocumentInfo)
class AdminDocumentInfo(admin.ModelAdmin):
    list_display = ['passport_id', 'iin', 'license_id', 'version', 'user']
