from django.contrib.contenttypes.models import ContentType
from rest_framework.serializers import SerializerMethodField, ListField, IntegerField, ValidationError, DictField, \
    RelatedField, SlugRelatedField
from django.utils.translation import ugettext_lazy as _
from .models import *
from auxiliaries.models import Attachment
from support.serializers import CustomSerializer, CustomModelSerializer

__author__ = 'slyfest'


class CountrySerializer(CustomModelSerializer):
    class Meta:
        model = Country
        fields = ('slug', 'title', 'latitude', 'longitude')


class CountryRelatedField(SlugRelatedField):
    def to_representation(self, obj):
        return CountrySerializer(obj).data


class TypeServiceSerializer(CustomModelSerializer):
    class Meta:
        model = TypeService
        fields = ('id', 'title')


class TypePlaceSerializer(CustomModelSerializer):
    class Meta:
        model = TypePlace
        fields = ('id', 'title')


class AddressSerializer(CustomModelSerializer):
    class Meta:
        model = Address
        fields = ('id', 'title', 'latitude', 'longitude')


class PlaceShortSerializer(CustomModelSerializer):
    place_type = SerializerMethodField()
    address = SerializerMethodField()
    place_latitude = SerializerMethodField()
    place_longitude = SerializerMethodField()
    info = SerializerMethodField()

    def get_place_type(self, place):
        place_type = TypePlace.objects.filter(type_places__service__services__place=place).distinct().first()
        place_type = dict(
            id=place_type.id,
            title=place_type.title
        )
        return place_type

    def get_address(self, place):
        return place.address.title

    def get_place_latitude(self, place):
        return place.address.address_coord.latitude

    def get_place_longitude(self, place):
        return place.address.address_coord.longitude

    def get_info(self, place):
        place_companies = PlaceCompany.objects.filter(place=place)
        operators = list()

        for place_company in place_companies:
            try:
                if place_company.company:
                    operator = dict(
                        id=place_company.company.id,
                        title=place_company.company.name,
                        site=place_company.company.site,
                        email=place_company.company.email,
                        phones=place_company.company.phone
                    )
                    operators.append(operator)
            except Company.DoesNotExist:
                pass
        try:
            if place.airports:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.AIRPORT_OPERATOR_LOGO_IMAGE_MIN_ID).url
                info = dict(
                    code_iata=place.airports.get().code_iata,
                    code_ikao=place.airports.get().code_ikao,
                    operators=operators
                )
                return info
        except Airport.DoesNotExist:
            pass
        try:
            if place.ports:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.PORT_OPERATOR_LOGO_IMAGE_MIN_ID).url
                info = dict(
                    unlocode=place.ports.get().unlocode,
                    operators=operators
                )
                return info
        except Port.DoesNotExist:
            pass

        try:
            if place.warehouses:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.WAREHOUSE_OPERATOR_LOGO_IMAGE_MIN_ID).url
                info = dict(
                    operators=operators,
                    phone=place.warehouses.get().phone
                )
                return info
        except Warehouse.DoesNotExist:
            pass

        try:
            if place.railways:
                info = dict(
                    uic=place.railways.get().uic
                )
                return info
        except Railway.DoesNotExist:
            pass

    class Meta:
        model = Place
        fields = ('id', 'title', 'place_type', 'address', 'place_latitude', 'place_longitude', 'info')


class PlaceDetailSerializer(CustomModelSerializer):
    place_type = SerializerMethodField()
    info = SerializerMethodField()
    address = SerializerMethodField()
    services = SerializerMethodField()
    service_type = SerializerMethodField()

    def get_place_type(self, place):
        place_company = PlaceCompany.objects.filter(place=place).distinct().first()
        place_type = place_company.service.type_service.type_place
        place_type = dict(
            id=place_type.id,
            title=place_type.title
        )
        return place_type

    def get_service_type(self, place):
        place_company = PlaceCompany.objects.filter(place=place).distinct().first()
        return place_company.service.type_service.title

    def get_services(self, place):
        place_company = PlaceCompany.objects.filter(place=place).distinct().first()
        services = list()
        service = place_company.service.title
        services.append(service)
        return services

    def get_address(self, place):
        address = place.address
        return dict(
            latitude=address.address_coord.latitude,
            longitude=address.address_coord.longitude,
            title=address.title,
            city=address.address_coord.city.title,
            country=address.address_coord.city.country.title,
            time=address.address_coord.city.get_time_from_timezone(city_timezone=address.address_coord.city.timezone)
        )

    def get_info(self, place):
        operators = list()
        place_companies = PlaceCompany.objects.filter(place=place)
        for place_company in place_companies:
            try:
                if place_company.company:
                    operator = dict(
                        id=place_company.company.id,
                        title=place_company.company.name,
                        phones=place_company.company.phone,
                        email=place_company.company.email,
                        site=place_company.company.site,
                    )
                    operators.append(operator)
            except Company.DoesNotExist:
                pass
        try:
            if place.airports:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.AIRPORT_OPERATOR_LOGO_IMAGE_ID).url
                phones = list()
                phones.append('228-48-45')
                phones.append('320-29-25')
                phones.append('250-12-36')
                return dict(
                    code_iata=place.airports.get().code_iata,
                    code_ikao=place.airports.get().code_ikao,
                    fax=place.airports.get().fax,
                    phones=phones,
                    site=place.airports.get().site,
                    email='test@mail.test',
                    operators=operators,
                )
        except Airport.DoesNotExist:
            pass

        try:
            if place.ports:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.PORT_OPERATOR_LOGO_IMAGE_ID).url
                return dict(
                    unlocode=place.ports.get().unlocode,
                    operators=operators
                )
        except Port.DoesNotExist:
            pass

        try:
            if place.warehouses:
                for operator in operators:
                    operator['logo'] = Attachment.objects.get(pk=settings.WAREHOUSE_OPERATOR_LOGO_IMAGE_ID).url
                return dict(
                    operators=operators,
                    phone=place.warehouses.get().phone
                )
        except Warehouse.DoesNotExist:
            pass

        try:
            if place.railways:
                return dict(
                    uic=place.railways.get().uic
                )
        except Railway.DoesNotExist:
            pass

    class Meta:
        model = Place
        fields = ('id', 'title', 'place_type', 'services', 'service_type', 'address', 'info')


class PlaceSerializer(CustomModelSerializer):
    service = ListField()
    type_services = ListField()
    place_type = ListField()
    airport = DictField()

    def validate_services(self, services_ids):
        services = list()
        for service_id in services_ids:
            service = Service.get_object(service_id)
            services.append(service)
        return services

    def validate_type_services(self, type_services_ids):
        type_services = list()
        for type_service_id in type_services_ids:
            type_service = TypeService.get_object(type_service_id)
            type_services.append(type_service)
        return type_services

    def validate_type_places(self, type_places_ids):
        type_places = list()
        for type_place_id in type_places_ids:
            type_place = TypePlace.get_object(type_place_id)
            type_places.append(type_place)
        return type_places

    class Meta:
        model = Place
        fields = ('id', 'title', 'user_address', 'service', 'type_services', 'type_places', 'airport')


class RequestPlaceDetailSerializer(CustomSerializer):
    place_id = IntegerField()

    def validate_place_id(self, place_id):
        try:
            place = Place.objects.get(id=place_id)
        except Place.DoesNotExist:
            raise ValidationError(_('place does not exist.'))


class RequestPlacesListSerializer(CustomSerializer):
    place_type_id = CharField()
    south_west_lat = FloatField()
    north_east_lat = FloatField()
    south_west_lng = FloatField()
    north_east_lng = FloatField()

    def validate_south_west_lat(self, south_west_lat):
        return south_west_lat

    def validate_north_east_lat(self, north_east_lat):
        return north_east_lat

    def validate_south_west_lng(self, south_west_lng):
        return south_west_lng

    def validate_north_east_lng(self, north_east_lng):
        return north_east_lng

    def validate_place_type_id(self, place_type_id):
        validated_list = list()
        for entry in place_type_id.split(','):
            try:
                place_type = TypePlace.objects.get(pk=int(entry))
                validated_list.append(place_type.pk)
            except TypePlace.DoesNotExist:
                raise ApiStandartException(dict(error_code='40100'))
        return validated_list


class RequestPlacesFromQuerySerializer(CustomSerializer):
    place_type_id = ListField()
    query = CharField()

    def validate_place_type_id(self, place_type_id):
        try:
            places_types = list()
            for place_type_value in place_type_id:
                place_type = TypePlace.objects.get(id=place_type_value)
                places_types.append(place_type.id)
        except TypePlace.DoesNotExist:
            raise ValidationError(_('place type does not exist.'))
        return places_types
