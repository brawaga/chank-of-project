# coding=utf-8

from datetime import datetime
from random import uniform
from django.db.models import Model, CharField, FloatField, \
    ForeignKey, TimeField, Q, NullBooleanField, PositiveSmallIntegerField, PositiveIntegerField, IntegerField, \
    SlugField, \
    BooleanField
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.conf import settings
from slugify import slugify
from support.exceptions import ApiStandartException
from pytz import timezone

import geocoder
import goslate
import chardet

try:
    User = settings.AUTH_USER_MODEL
except ImportError:
    from django.contrib.auth.models import User


class Country(Model):
    slug = SlugField(max_length=256, primary_key=True, db_index=True, verbose_name=_(u'iso slug'))
    title = CharField(max_length=256, verbose_name=_(u'title'))
    latitude = FloatField(null=True, blank=True, verbose_name=_(u'latitude'))
    longitude = FloatField(null=True, blank=True, verbose_name=_(u'longitude'))
    in_profile = NullBooleanField(null=True, blank=False)

    countries_codes = {
        "BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria",
        "BA": "Bosnia and Herzegovina", "BB": "Barbados", "WF": "Wallis and Futuna", "BL": "Saint Barthelemy",
        "BM": "Bermuda", "BN": "Brunei", "BO": "Bolivia", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin",
        "BT": "Bhutan", "JM": "Jamaica", "BV": "Bouvet Island", "BW": "Botswana", "WS": "Samoa",
        "BQ": "Bonaire, Saint Eustatius and Saba ", "BR": "Brazil", "BS": "Bahamas", "JE": "Jersey",
        "BY": "Belarus", "BZ": "Belize", "RU": "Russia", "RW": "Rwanda", "RS": "Serbia", "TL": "East Timor",
        "RE": "Reunion", "TM": "Turkmenistan", "TJ": "Tajikistan", "RO": "Romania", "TK": "Tokelau",
        "GW": "Guinea-Bissau", "GU": "Guam", "GT": "Guatemala",
        "GS": "South Georgia and the South Sandwich Islands", "GR": "Greece", "GQ": "Equatorial Guinea",
        "GP": "Guadeloupe", "JP": "Japan", "GY": "Guyana", "GG": "Guernsey", "GF": "French Guiana",
        "GE": "Georgia", "GD": "Grenada", "GB": "United Kingdom", "GA": "Gabon", "SV": "El Salvador",
        "GN": "Guinea", "GM": "Gambia", "GL": "Greenland", "GI": "Gibraltar", "GH": "Ghana", "OM": "Oman",
        "TN": "Tunisia", "JO": "Jordan", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "HK": "Hong Kong",
        "HN": "Honduras", "HM": "Heard Island and McDonald Islands", "VE": "Venezuela", "PR": "Puerto Rico",
        "PS": "Palestinian Territory", "PW": "Palau", "PT": "Portugal", "SJ": "Svalbard and Jan Mayen",
        "PY": "Paraguay", "IQ": "Iraq", "PA": "Panama", "PF": "French Polynesia", "PG": "Papua New Guinea",
        "PE": "Peru", "PK": "Pakistan", "PH": "Philippines", "PN": "Pitcairn", "PL": "Poland",
        "PM": "Saint Pierre and Miquelon", "ZM": "Zambia", "EH": "Western Sahara", "EE": "Estonia", "EG": "Egypt",
        "ZA": "South Africa", "EC": "Ecuador", "IT": "Italy", "VN": "Vietnam", "SB": "Solomon Islands",
        "ET": "Ethiopia", "SO": "Somalia", "ZW": "Zimbabwe", "SA": "Saudi Arabia", "ES": "Spain", "ER": "Eritrea",
        "ME": "Montenegro", "MD": "Moldova", "MG": "Madagascar", "MF": "Saint Martin", "MA": "Morocco",
        "MC": "Monaco", "UZ": "Uzbekistan", "MM": "Myanmar", "ML": "Mali", "MO": "Macao", "MN": "Mongolia",
        "MH": "Marshall Islands", "MK": "Macedonia", "MU": "Mauritius", "MT": "Malta", "MW": "Malawi",
        "MV": "Maldives", "MQ": "Martinique", "MP": "Northern Mariana Islands", "MS": "Montserrat",
        "MR": "Mauritania", "IM": "Isle of Man", "UG": "Uganda", "TZ": "Tanzania", "MY": "Malaysia",
        "MX": "Mexico", "IL": "Israel", "FR": "France", "IO": "British Indian Ocean Territory",
        "SH": "Saint Helena", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands", "FM": "Micronesia",
        "FO": "Faroe Islands", "NI": "Nicaragua", "NL": "Netherlands", "NO": "Norway", "NA": "Namibia",
        "VU": "Vanuatu", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria",
        "NZ": "New Zealand", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "CK": "Cook Islands", "XK": "Kosovo",
        "CI": "Ivory Coast", "CH": "Switzerland", "CO": "Colombia", "CN": "China", "CM": "Cameroon", "CL": "Chile",
        "CC": "Cocos Islands", "CA": "Canada", "CG": "Republic of the Congo", "CF": "Central African Republic",
        "CD": "Democratic Republic of the Congo", "CZ": "Czech Republic", "CY": "Cyprus", "CX": "Christmas Island",
        "CR": "Costa Rica", "CW": "Curacao", "CV": "Cape Verde", "CU": "Cuba", "SZ": "Swaziland", "SY": "Syria",
        "SX": "Sint Maarten", "KG": "Kyrgyzstan", "KE": "Kenya", "SS": "South Sudan", "SR": "Suriname",
        "KI": "Kiribati", "KH": "Cambodia", "KN": "Saint Kitts and Nevis", "KM": "Comoros",
        "ST": "Sao Tome and Principe", "SK": "Slovakia", "KR": "South Korea", "SI": "Slovenia",
        "KP": "North Korea", "KW": "Kuwait", "SN": "Senegal", "SM": "San Marino", "SL": "Sierra Leone",
        "SC": "Seychelles", "KZ": "Kazakhstan", "KY": "Cayman Islands", "SG": "Singapore", "SE": "Sweden",
        "SD": "Sudan", "DO": "Dominican Republic", "DM": "Dominica", "DJ": "Djibouti", "DK": "Denmark",
        "VG": "British Virgin Islands", "DE": "Germany", "YE": "Yemen", "DZ": "Algeria", "US": "United States",
        "UY": "Uruguay", "YT": "Mayotte", "UM": "United States Minor Outlying Islands", "LB": "Lebanon",
        "LC": "Saint Lucia", "LA": "Laos", "TV": "Tuvalu", "TW": "Taiwan", "TT": "Trinidad and Tobago",
        "TR": "Turkey", "LK": "Sri Lanka", "LI": "Liechtenstein", "LV": "Latvia", "TO": "Tonga", "LT": "Lithuania",
        "LU": "Luxembourg", "LR": "Liberia", "LS": "Lesotho", "TH": "Thailand",
        "TF": "French Southern Territories", "TG": "Togo", "TD": "Chad", "TC": "Turks and Caicos Islands",
        "LY": "Libya", "VA": "Vatican", "VC": "Saint Vincent and the Grenadines", "AE": "United Arab Emirates",
        "AD": "Andorra", "AG": "Antigua and Barbuda", "AF": "Afghanistan", "AI": "Anguilla",
        "VI": "U.S. Virgin Islands", "IS": "Iceland", "IR": "Iran", "AM": "Armenia", "AL": "Albania",
        "AO": "Angola", "AQ": "Antarctica", "AS": "American Samoa", "AR": "Argentina", "AU": "Australia",
        "AT": "Austria", "AW": "Aruba", "IN": "India", "AX": "Aland Islands", "AZ": "Azerbaijan", "IE": "Ireland",
        "ID": "Indonesia", "UA": "Ukraine", "QA": "Qatar", "MZ": "Mozambique"
    }

    class Meta:
        verbose_name = _(u'country')
        verbose_name_plural = _(u'countries')

    def __unicode__(self):
        return u'country - {0}'.format(self.title)

    @classmethod
    def get_by_id(cls, country_id):
        try:
            return cls.objects.get(pk=country_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400023))


class City(Model):
    slug = SlugField(max_length=256, primary_key=True, db_index=True, unique=True, verbose_name=_(u'slug'))
    title = CharField(max_length=256, verbose_name=_(u'name'))
    timezone = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'timezone'))
    country = ForeignKey(Country, verbose_name=_(u'countries'))
    latitude = FloatField(null=True, blank=True, verbose_name=_(u'latitude'))
    longitude = FloatField(null=True, blank=True, verbose_name=_(u'longitude'))

    class Meta:
        verbose_name = _(u'city')
        verbose_name_plural = _(u'cities')

        ordering = ['title']

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @classmethod
    def get_time_from_timezone(cls, city_timezone):
        if city_timezone:
            time = datetime.now(timezone(city_timezone))
            time_clean = time.strftime('%H:%M')
            return time_clean


class AddressCoord(Model):
    city = ForeignKey(City, blank=True, null=True, verbose_name=_(u'city'))
    latitude = FloatField(verbose_name=_(u'latitude'))
    longitude = FloatField(verbose_name=_(u'longitude'))
    is_correct = BooleanField()

    class Meta:
        verbose_name = _(u'address coord')
        verbose_name_plural = _(u'addresses coord')
        unique_together = ('latitude', 'longitude')

    def __unicode__(self):
        return u'address coord - {0}, {1}'.format(self.latitude, self.longitude)


class Address(Model):
    title = CharField(max_length=256, db_index=True, verbose_name=_(u'street and house number'))
    address_coord = ForeignKey(AddressCoord, db_index=True, verbose_name=_(u'address coord'))

    class Meta:
        verbose_name = _(u'address')
        verbose_name_plural = _(u'addresses')
        unique_together = (('title', 'address_coord'), )

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @staticmethod
    def make(params, geocode_from):
        # geocode_from: 'address', 'coord'
        # if 'address' params - {'address':'test address'}
        # if 'coord' params - {'latitude':54.3, 'longitude':33.2}

        gs = goslate.Goslate()

        geocode_methods = dict(
            address=u'geocoder.{0}(params["address"])',
            coord=u'geocoder.{0}([params["latitude"], params["longitude"]], method="reverse")'
        )
        geocode_services = ['google', 'yandex', 'mapquest']
        geocode_services_errors = ['ZERO_RESULTS', 'ERROR - No results found']

        is_geocoded = False
        for service in geocode_services:
            g = eval(geocode_methods[geocode_from].format(service))
            if g.status not in geocode_services_errors:
                country_title = g.country_long if service == 'google' else g.country
                country_code = g.country if service in ['google', 'mapquest'] else g.country_code
                city_title = g.city
                address_title = g.address
                if country_title:
                    if service == 'mapquest':
                        country_title = Country.countries_codes[
                            country_title.upper()] if country_title.upper() in Country.countries_codes else country_title
                    if not city_title and service == 'mapquest':
                        address_title = country_title

                    try:
                        address_ru_translation = gs.translate(address_title, 'ru')
                    except:
                        address_ru_translation = None

                    address_latitude = g.lat if geocode_from == 'address' else params['latitude']
                    address_longitude = g.lng if geocode_from == 'address' else params['longitude']

                    is_geocoded = True
                    break
        if not is_geocoded:
            raise ApiStandartException(dict(error_code=400058))

        is_correct_data = False
        city = None

        if country_title and city_title:
            c = geocoder.mapquest(country_title)
            country_latitude = c.lat
            country_longitude = c.lng

            f = geocoder.mapquest(city_title)
            city_latitude = f.lat
            city_longitude = f.lng
            if city_latitude and city_longitude:
                try:
                    country_ru_translation = gs.translate(country_title, 'ru')
                except:
                    country_ru_translation = None

                try:
                    city_ru_translation = gs.translate(city_title, 'ru')
                except:
                    city_ru_translation = None

                country, is_created = Country.objects.get_or_create(
                    slug=slugify(country_code, to_lower=True),
                    defaults=dict(
                        latitude=country_latitude,
                        longitude=country_longitude,
                        title=country_title,
                        title_en=country_title,
                        title_ru=country_ru_translation
                    )

                )
                encoding = chardet.detect(city_title)['encoding']
                if encoding != 'utf-8':
                    city_title = city_title.decode(encoding, 'replace').encode('utf-8')

                city, is_created = City.objects.get_or_create(
                    slug=slugify('{0} {1} {2}'.format(
                        city_title,
                        int(city_latitude), int(city_longitude)), to_lower=True),
                    defaults=dict(
                        country=country,
                        title=city_title,
                        latitude=city_latitude,
                        longitude=city_longitude,
                        title_en=city_title,
                        title_ru=city_ru_translation)
                )
                is_correct_data = True

        address_coord, is_created = AddressCoord.objects.get_or_create(
            latitude=address_latitude,
            longitude=address_longitude,
            defaults=dict(
                is_correct=is_correct_data,
                city=city
            )
        )
        address, is_created = Address.objects.get_or_create(
            title_en=address_title,
            address_coord=address_coord,
            defaults=dict(
                title=address_title,
                # title_en=address_title,
                title_ru=address_ru_translation
            )
        )

        return address

    @classmethod
    def make_from_string(cls, address):
        return cls.make(dict(address=address), 'address')

    @classmethod
    def make_from_coord(cls, latitude, longitude):
        return cls.make(dict(latitude=latitude, longitude=longitude), 'coord')

    def get_limited(self, is_limited):
        if is_limited:
            if self.address_coord.city:
                return dict(
                    address=u'{0}, {1}'.format(self.address_coord.city.country.title, self.address_coord.city.title),
                    latitude=self.address_coord.latitude + uniform(0.01, 0.02),
                    longitude=self.address_coord.longitude + uniform(0.01, 0.02)
                )
            else:
                return dict(
                    address=self.title.split(',')[0],
                    latitude=self.address_coord.latitude + uniform(0.01, 0.02),
                    longitude=self.address_coord.longitude + uniform(0.01, 0.02)
                )
        else:
            return dict(
                address=self.address_coord.title,
                latitude=self.address_coord.latitude,
                longitude=self.address_coord.longitude
            )


class Place(Model):
    title = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'title'))
    address = ForeignKey(Address, null=True, blank=True, related_name='address_places', verbose_name=_(u'address'))
    user_address = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'user_address'))

    latitude = FloatField(default=0, verbose_name=_(u'latitude'))
    longitude = FloatField(default=0, verbose_name=_(u'longitude'))

    class Meta:
        verbose_name = _(u'place')
        verbose_name_plural = _(u'places')

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "title__icontains", )

    @classmethod
    def place_search(cls, query, place_list=None, places_types=None):
        query = query.split()
        compiled = reduce(Q.__and__, [Q(title__icontains=word) for word in query])
        if place_list:
            entries = place_list.filter(compiled,
                                        Q(places__service__type_service__type_place__in=places_types)).distinct()
        else:
            if places_types:
                entries = Place.objects.filter(compiled,
                                               Q(places__service__type_service__type_place__in=places_types)).distinct()
            else:
                entries = Place.objects.filter(compiled)
        return entries

    @classmethod
    def get_in_square(cls, places_types, south_west_lat, south_west_lng, north_east_lat, north_east_lng):
        place_list = cls.objects.filter(
            Q(places__service__type_service__type_place__in=places_types),
            Q(address__address_coord__latitude__gte=south_west_lat),
            Q(address__address_coord__latitude__lte=north_east_lat),
            Q(address__address_coord__longitude__gte=south_west_lng),
            Q(address__address_coord__longitude__lte=north_east_lng)
        ).distinct().order_by('id')
        return place_list


@receiver(pre_save, sender=Place)
def model_pre_save(sender, instance, *args, **kwargs):
    is_changed = False
    if not instance.id:
        is_changed = True
    else:
        place = Place.objects.get(pk=instance.id)
        if place.address.address_coord.latitude != instance.address.address_coord.latitude or \
                        place.address.address_coord.longitude != instance.address.address_coord.longitude:
            is_changed = True
    if is_changed:
        address = Address.make_from_coord(instance.latitude, instance.longitude)
        instance.address = address


class Airport(Model):
    place = ForeignKey(Place, related_name='airports', null=True, blank=True, editable=True, verbose_name=_(u'place'))
    code_iata = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'code_iata'))
    code_ikao = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'code_ikao'))
    phones = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'phones'))
    fax = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'fax'))
    site = CharField(max_length=512, null=True, blank=False, verbose_name=_(u'site'))
    rating = IntegerField(verbose_name=_(u'rating'))

    class Meta:
        verbose_name = _(u'airport')
        verbose_name_plural = _(u'airports')
        ordering = ['rating']

    def __unicode__(self):
        return u'{0}'.format(self.code_iata)


class Port(Model):
    place = ForeignKey(Place, related_name='ports', verbose_name=_(u'place'))
    unlocode = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'unlocode'))
    rating = IntegerField(verbose_name=_(u'rating'), default=0)

    class Meta:
        verbose_name = _(u'port')
        verbose_name_plural = _(u'ports')

    def __unicode__(self):
        return u'{0}'.format(self.unlocode)


class Railway(Model):
    place = ForeignKey(Place, related_name='railways', verbose_name=_(u'place'))
    uic = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'uic'))
    rating = IntegerField(verbose_name=_(u'rating'), default=0)


class Warehouse(Model):
    TYPES = (
        (0, _(u'open temporary storage warehouse')),
        (1, _(u'closed temporary storage warehouse')),
        (2, _(u'storage complex')),
    )

    place = ForeignKey(Place, related_name='warehouses', verbose_name=_(u'place'))
    internal_constructions = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'internal_constructions'))
    floor = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'floor'))
    ventilation = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'ventilation'))
    temperature = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'temperature'))
    security = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'security'))
    communications = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'communications'))
    loading_constructions = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'loading_constructions'))
    store_rooms = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'store_rooms'))
    telecommunications = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'telecommunications'))
    control_and_audit = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'control_and_audit'))
    territory = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'territory'))
    parking = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'parking'))
    railway = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'railway'))
    capacity = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'capacity'))
    phone = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'phone'))
    type = PositiveSmallIntegerField(choices=TYPES, verbose_name=_(u'warehouse types'))
    rating = IntegerField(verbose_name=_(u'rating'), default=0)

    class Meta:
        verbose_name = _(u'warehouse')
        verbose_name_plural = _(u'warehouses')

    def __unicode__(self):
        return u'{0}'.format(self.place.title)


class Customs(Model):
    TYPES = (
        (0, _(u'customs center')),
        (1, _(u'customs post')),
    )

    place = ForeignKey(Place, related_name='customs', verbose_name=_(u'customs'))
    type = PositiveIntegerField(choices=TYPES, verbose_name=_(u'customs types'))
    phone = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'phone'))
    fax = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'fax'))
    site = CharField(max_length=512, null=True, blank=True, verbose_name=_(u'site'))
    email = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'email'))
    rating = IntegerField(verbose_name=_(u'rating'), default=0)

    class Meta:
        verbose_name = _(u'customs')
        verbose_name_plural = _(u'customs')

    def __unicode__(self):
        return u'{0}'.format(self.place.title)


class TypePlace(Model):
    title = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'title'))

    class Meta:
        verbose_name = _(u'type_place')
        verbose_name_plural = _(u'type_places')

    def __unicode__(self):
        return u'{0}'.format(self.title)

    @classmethod
    def get_object(cls, place_type_id):
        try:
            return cls.objects.get(place_type_id)
        except cls.DoesNotExist:
            raise ApiStandartException(dict(error_code=400006))


class TypeService(Model):
    title = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'title'))
    type_place = ForeignKey(TypePlace, related_name='type_places', verbose_name=_(u'type_place'))

    class Meta:
        verbose_name = _(u'type_service')
        verbose_name_plural = _(u'type_services')

    def __unicode__(self):
        return u'{0}'.format(self.title)


class Service(Model):
    title = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'title'))
    type_service = ForeignKey(TypeService, verbose_name=_(u'type_service'))

    class Meta:
        verbose_name = _(u'service')
        verbose_name_plural = _(u'services')

    def __unicode__(self):
        return u'{0}'.format(self.title)


class Company(Model):
    name = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'name'))
    address = ForeignKey(Address, blank=True, null=True, verbose_name=_(u'address'))
    user_address = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'address'))
    phone = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'phone'))
    email = CharField(max_length=128, null=True, blank=True, verbose_name=_(u'email'))
    site = CharField(max_length=512, null=True, blank=True, verbose_name=_(u'site'))

    class Meta:
        verbose_name = _(u'company')
        verbose_name_plural = _(u'companies')

    def __unicode__(self):
        return u'{0}'.format(self.name)


class PlaceCompany(Model):
    from company.models import CompanyInfo
    company = ForeignKey(Company, null=True, blank=True, verbose_name=_(u'company'))
    company_info = ForeignKey(CompanyInfo, null=True, blank=True, verbose_name=_(u'company_info'))
    service = ForeignKey(Service, null=True, blank=True, related_name='services', verbose_name=_(u'service'))
    service_value = CharField(max_length=256, null=True, blank=True, verbose_name=_(u'service_value'))
    place = ForeignKey(Place, related_name='places', verbose_name=_(u'place'))

    class Meta:
        verbose_name = _(u'place_company')
        verbose_name_plural = _(u'place_companies')


class Phone(Model):
    phone = CharField(max_length=64, null=True, blank=False, verbose_name=_(u'phone'))
    type = CharField(max_length=128, null=True, blank=False, verbose_name=_(u'type'))
    place = ForeignKey(Place, verbose_name=_(u'place'))

    class Meta:
        verbose_name = _(u'phone')
        verbose_name_plural = _(u'phones')

    def __unicode__(self):
        return u'{0}'.format(self.phone)


class Email(Model):
    email = CharField(max_length=128, null=True, blank=False, verbose_name=_(u'email'))
    place = ForeignKey(Place, verbose_name=_(u'place'))

    class Meta:
        verbose_name = _(u'email')
        verbose_name_plural = _(u'emails')

    def __unicode__(self):
        return u'{0}'.format(self.email)


class Site(Model):
    url = CharField(max_length=512, null=True, blank=False, verbose_name=_(u'url'))
    place = ForeignKey(Place, verbose_name=_(u'place'))

    class Meta:
        verbose_name = _(u'site')
        verbose_name_plural = _(u'sites')

    def __unicode__(self):
        return u'{0}'.format(self.url)


class WorkTime(Model):
    week_day = CharField(max_length=256, null=True, blank=False, verbose_name=_(u'week_day'))
    start_time = TimeField(verbose_name=_(u'start_time'))
    end_time = TimeField(verbose_name=_(u'end_time'))
    place = ForeignKey(Place, verbose_name=_(u'place'))

    class Meta:
        verbose_name = _(u'work_time')
        verbose_name_plural = _(u'work_times')

    def __unicode__(self):
        return u'{0}'.format(self.week_day)
