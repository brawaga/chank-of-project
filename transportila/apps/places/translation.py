from modeltranslation.translator import register, TranslationOptions
from .models import Address, City, Country

__author__ = 'sandan'


@register(Address)
class AddressTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(City)
class CityTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('title', )
