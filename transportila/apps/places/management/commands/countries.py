# -*- coding: utf-8 -*-
from ...models import Country, City, Address


def get_countries():
    return [
        {
            "id": 1,
            "name_ru": "Казахстан",
            "name": "Kazakhstan"
        },
        {
            "id": 2,
            "name_ru": "России",
            "name": "Russia"
        },
        {
            "id": 3,
            "name_ru": "Латвия",
            "name": "Latvia"
        },
        {
            "id": 4,
            "name_ru": "Украина",
            "name": "Ukraine"
        },
        {
            "id": 5,
            "name_ru": "Ангола",
            "name": "Angola"
        },
        {
            "id": 6,
            "name_ru": "Аргентина",
            "name": "Argentina"
        },
        {
            "id": 7,
            "name_ru": "Аруба",
            "name": "Aruba"
        },
        {
            "id": 8,
            "name_ru": "Австралия",
            "name": "Australia"
        },
        {
            "id": 9,
            "name_ru": "Австрия",
            "name": "Austria"
        },
        {
            "id": 10,
            "name_ru": "Азербайджан",
            "name": "Azerbaijan"
        },
        {
            "id": 11,
            "name_ru": "Багамские острова",
            "name": "Bahamas"
        },
        {
            "id": 12,
            "name_ru": "Бахрейн",
            "name": "Bahrain"
        },
        {
            "id": 13,
            "name_ru": "Бельгия",
            "name": "Belgium"
        },
        {
            "id": 14,
            "name_ru": "Бенин",
            "name": "Benin"
        },
        {
            "id": 15,
            "name_ru": "Боливия",
            "name": "Bolivia"
        },
        {
            "id": 16,
            "name_ru": "Бонайре",
            "name": "Bonaire"
        },
        {
            "id": 17,
            "name_ru": "Босния-Герцеговина",
            "name": "Bosnia-Herzegovina"
        },
        {
            "id": 18,
            "name_ru": "Ботсвана",
            "name": "Botswana"
        },
        {
            "id": 19,
            "name_ru": "Бразилия",
            "name": "Brazil"
        },
        {
            "id": 20,
            "name_ru": "Бруней",
            "name": "Brunei"
        },
        {
            "id": 21,
            "name_ru": "Болгария",
            "name": "Bulgaria"
        },
        {
            "id": 22,
            "name_ru": "Камбоджа",
            "name": "Cambodia"
        },
        {
            "id": 23,
            "name_ru": "Камерун",
            "name": "Cameroon"
        },
        {
            "id": 24,
            "name_ru": "Канада",
            "name": "Canada"
        },
        {
            "id": 25,
            "name_ru": "Чили",
            "name": "Chile"
        },
        {
            "id": 26,
            "name_ru": "Китай(Фучжоу)",
            "name": "China(Fuzhou)"
        },
        {
            "id": 27,
            "name_ru": "Китай(Шанхай)",
            "name": "China(Shanghai)"
        },
        {
            "id": 28,
            "name_ru": "Китай(Чжуншань)",
            "name": "China(Zhongshan)"
        },
        {
            "id": 29,
            "name_ru": "Колумбия",
            "name": "Colombia"
        },
        {
            "id": 30,
            "name_ru": "Коста-Рика",
            "name": "Costa Rica"
        },
        {
            "id": 31,
            "name_ru": "Хорватия",
            "name": "Croatia"
        },
        {
            "id": 32,
            "name_ru": "Кюрасао",
            "name": "Curacao"
        },
        {
            "id": 33,
            "name_ru": "Кипр",
            "name": "Cyprus"
        },
        {
            "id": 34,
            "name_ru": "Чешская Республика",
            "name": "Czech Republic"
        },
        {
            "id": 35,
            "name_ru": "Демократическая Республика Конго",
            "name": "Democratic Republic of Congo"
        },
        {
            "id": 36,
            "name_ru": "Дания",
            "name": "Denmark"
        },
        {
            "id": 37,
            "name_ru": "Восточная Африка",
            "name": "East Africa"
        },
        {
            "id": 38,
            "name_ru": "Эквадоре",
            "name": "Ecuador"
        },
        {
            "id": 39,
            "name_ru": "Сальвадор",
            "name": "El Salvador"
        },
        {
            "id": 40,
            "name_ru": "Сальвадор-Сан-Мигель",
            "name": "El Salvador-San Miguel"
        },
        {
            "id": 41,
            "name_ru": "Эстония",
            "name": "Estonia"
        },
        {
            "id": 42,
            "name_ru": "Фиджи",
            "name": "Fiji"
        },
        {
            "id": 43,
            "name_ru": "Финляндия",
            "name": "Finland"
        },
        {
            "id": 44,
            "name_ru": "Франция",
            "name": "France"
        },
        {
            "id": 45,
            "name_ru": "Габон",
            "name": "Gabon"
        },
        {
            "id": 46,
            "name_ru": "Германия",
            "name": "Germany"
        },
        {
            "id": 47,
            "name_ru": "Гана",
            "name": "Ghana"
        },
        {
            "id": 48,
            "name_ru": "Великобритания",
            "name": "Great Britain"
        },
        {
            "id": 49,
            "name_ru": "Греция",
            "name": "Greece"
        },
        {
            "id": 50,
            "name_ru": "Гваделупа",
            "name": "Guadaloupe"
        },
        {
            "id": 51,
            "name_ru": "Гватемала",
            "name": "Guatemala"
        },
        {
            "id": 52,
            "name_ru": "Гондурас",
            "name": "Honduras"
        },
        {
            "id": 53,
            "name_ru": "Гондурас",
            "name": "Honduras"
        },
        {
            "id": 54,
            "name_ru": "Гонконг",
            "name": "Hong Kong"
        },
        {
            "id": 55,
            "name_ru": "Венгрия",
            "name": "Hungary"
        },
        {
            "id": 56,
            "name_ru": "Исландия",
            "name": "Iceland"
        },
        {
            "id": 57,
            "name_ru": "Индия",
            "name": "India"
        },
        {
            "id": 58,
            "name_ru": "Индонезия",
            "name": "Indonesia"
        },
        {
            "id": 59,
            "name_ru": "Ирландия",
            "name": "Ireland"
        },
        {
            "id": 60,
            "name_ru": "Израиль",
            "name": "Israel"
        },
        {
            "id": 61,
            "name_ru": "Италия",
            "name": "Italy"
        },
        {
            "id": 62,
            "name_ru": "Кот-д'Ивуар",
            "name": "IvoryCoast"
        },
        {
            "id": 63,
            "name_ru": "Япония",
            "name": "Japan"
        },
        {
            "id": 64,
            "name_ru": "Корея",
            "name": "Korea"
        },
        {
            "id": 65,
            "name_ru": "Косово",
            "name": "Kosovo"
        },
        {
            "id": 66,
            "name_ru": "Кувейт",
            "name": "Kuwait"
        },
        {
            "id": 67,
            "name_ru": "Лесото",
            "name": "Lesotho"
        },
        {
            "id": 68,
            "name_ru": "Литва",
            "name": "Lithuania"
        },
        {
            "id": 69,
            "name_ru": "Люксембург",
            "name": "Luxembourg"
        },
        {
            "id": 70,
            "name_ru": "Макао",
            "name": "Macau"
        },
        {
            "id": 71,
            "name_ru": "Македония",
            "name": "Macedonia"
        },
        {
            "id": 72,
            "name_ru": "Малайзия",
            "name": "Malaysia"
        },
        {
            "id": 73,
            "name_ru": "Мартиника",
            "name": "Martinique"
        },
        {
            "id": 74,
            "name_ru": "Мавритания",
            "name": "Mauritania"
        },
        {
            "id": 75,
            "name_ru": "Мексика",
            "name": "Mexico"
        },
        {
            "id": 77,
            "name_ru": "Намибия",
            "name": "Namibia"
        },
        {
            "id": 78,
            "name_ru": "Нидерланды",
            "name": "Netherlands"
        },
        {
            "id": 80,
            "name_ru": "Никарагуа",
            "name": "Nicaragua"
        },
        {
            "id": 81,
            "name_ru": "Нигерия",
            "name": "Nigeria"
        },
        {
            "id": 82,
            "name_ru": "Северная Ирландия",
            "name": "Northern Ireland"
        },
        {
            "id": 83,
            "name_ru": "Норвегия",
            "name": "Norway"
        },
        {
            "id": 84,
            "name_ru": "Панама",
            "name": "Panama"
        },
        {
            "id": 85,
            "name_ru": "Парагвай",
            "name": "Paraguay"
        },
        {
            "id": 86,
            "name_ru": "Перу",
            "name": "Peru"
        },
        {
            "id": 88,
            "name_ru": "Польша",
            "name": "Poland"
        },
        {
            "id": 89,
            "name_ru": "Португалия",
            "name": "Portugal"
        },
        {
            "id": 90,
            "name_ru": "Пуэрто-Рико",
            "name": "Puerto Rico"
        },
        {
            "id": 91,
            "name_ru": "Катар",
            "name": "Qatar"
        },
        {
            "id": 93,
            "name_ru": "Реюньон островов",
            "name": "Reunion Islands"
        },
        {
            "id": 94,
            "name_ru": "Румынии",
            "name": "Romania"
        },
        {
            "id": 95,
            "name_ru": "Руанде",
            "name": "Rwanda"
        },
        {
            "id": 96,
            "name_ru": "Саба",
            "name": "Saba"
        },
        {
            "id": 97,
            "name_ru": "Самоа",
            "name": "Samoa"
        },
        {
            "id": 99,
            "name_ru": "Сенегал",
            "name": "Senegal"
        },
        {
            "id": 100,
            "name_ru": "Сербия",
            "name": "Serbia"
        },
        {
            "id": 101,
            "name_ru": "Сингапур",
            "name": "Singapore"
        },
        {
            "id": 102,
            "name_ru": "Словакия",
            "name": "Slovak Republic"
        },
        {
            "id": 103,
            "name_ru": "Словения",
            "name": "Slovenia"
        },
        {
            "id": 104,
            "name_ru": "Южная Африка",
            "name": "South Africa"
        },
        {
            "id": 105,
            "name_ru": "Испания",
            "name": "Spain"
        },
        {
            "id": 107,
            "name_ru": "Сен-Мартен",
            "name": "St.Maarten"
        },
        {
            "id": 108,
            "name_ru": "Суринам",
            "name": "Suriname"
        },
        {
            "id": 109,
            "name_ru": "Свазиленд",
            "name": "Swaziland"
        },
        {
            "id": 110,
            "name_ru": "Швеция",
            "name": "Sweden"
        },
        {
            "id": 111,
            "name_ru": "Швейцария",
            "name": "Switzerland"
        },
        {
            "id": 113,
            "name_ru": "Танзания",
            "name": "Tanzania"
        },
        {
            "id": 114,
            "name_ru": "Таиланд",
            "name": "Thailand"
        },
        {
            "id": 115,
            "name_ru": "Того",
            "name": "Togo"
        },
        {
            "id": 116,
            "name_ru": "Турция",
            "name": "Turkey"
        },
        {
            "id": 117,
            "name_ru": "Уганда",
            "name": "Uganda"
        },
        {
            "id": 119,
            "name_ru": "США",
            "name": "United States"
        },
        {
            "id": 120,
            "name_ru": "Уругвай",
            "name": "Uruguay"
        },
        {
            "id": 121,
            "name_ru": "Венесуэла",
            "name": "Venezuela"
        },
        {
            "id": 122,
            "name_ru": "Вьетнам",
            "name": "Vietnam"
        },
        {
            "id": 123,
            "name_ru": "Черногория",
            "name": "Montenegro"
        },
        {
            "id": 76,
            "name_ru": "Молдова",
            "name": "Moldova"
        },
        {
            "id": 79,
            "name_ru": "Новая Зеландия",
            "name": "New Zealand"
        },
        {
            "id": 87,
            "name_ru": "Филиппины",
            "name": "Philippines"
        },
        {
            "id": 92,
            "name_ru": "Республика Конго",
            "name": "Republic of Congo"
        },
        {
            "id": 98,
            "name_ru": "Саудовская Аравия",
            "name": "Saudi Arabia"
        },
        {
            "id": 106,
            "name_ru": "Синт-Эстатиус",
            "name": "St.Eustatius"
        },
        {
            "id": 112,
            "name_ru": "Тайвань",
            "name": "Taiwan"
        },
        {
            "id": 118,
            "name_ru": "Объединенные Арабские Эмираты",
            "name": "United Arab Emirates"
        }
    ]


def add():
    country = add_country()
    city = add_city(country)
    add_address(city)


def add_country():
    country, is_created = Country.objects.get_or_create(title=u'Test country')
    return country


def add_city(country):
    city, is_created = City.objects.get_or_create(title=u'Test city', country=country)
    return city


def add_address(city):
    address, is_created = Address.objects.get_or_create(title=u'Test address', city=city, latitude=23.2, longitude=11.2)
    return address