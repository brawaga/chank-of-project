import os
from django.core.management.base import BaseCommand
import json
import goslate

from places.models import City, Country, Address

__author__ = 'sandan'


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'places' app\n")

        with open(os.path.join(os.path.dirname(__file__), 'data', 'fixtures_places_2015_9_28.json')) as data_file:
            data = json.load(data_file)

        countries = list()
        cities = list()
        addresses = list()

        gs = goslate.Goslate()

        for item in data:
            # if item['model'] == 'places.country' and isinstance(item['fields']['title'], unicode):
            #     try:
            #         print(u'country {0} - {1}'.format(item['fields']['title'], gs.translate(item['fields']['title'], 'ru')))
            #     except:
            #         continue
            #     countries.append(item)
            # elif item['model'] == 'places.city' and isinstance(item['fields']['title'], unicode):
            #     try:
            #         print(u'city {0} - {1}'.format(item['fields']['title'], gs.translate(item['fields']['title'], 'ru')))
            #     except:
            #         continue
            #     cities.append(item)
            if item['model'] == 'places.address' and isinstance(item['fields']['title'], unicode):
                try:
                    print(u'address {0} - {1}'.format(item['fields']['title'], gs.translate(item['fields']['title'], 'ru')))
                except:
                    continue
                addresses.append(item)

        for country in countries:
            try:
                ru_translation = gs.translate(country['fields']['title'], 'ru')
            except:
                continue
            c, is_created = Country.objects.get_or_create(
                id=country['pk'],
                latitude=country['fields']['latitude'],
                longitude=country['fields']['longitude'],
                defaults=dict(
                    title=country['fields']['title'],
                    title_en=country['fields']['title'],
                    title_ru=ru_translation
                )
            )
            if not is_created:
                c.title = country['fields']['title']
                c.title_en = country['fields']['title']
                c.title_ru = ru_translation
                c.save()

        for city in cities:
            try:
                ru_translation = gs.translate(city['fields']['title'], 'ru')
            except:
                continue
            c, is_created = City.objects.get_or_create(
                id=city['pk'],
                latitude=city['fields']['latitude'],
                longitude=city['fields']['longitude'],
                timezone=city['fields']['timezone'],
                country_id=city['fields']['country'],
                defaults=dict(
                    title=city['fields']['title'],
                    title_en=city['fields']['title'],
                    title_ru=ru_translation
                ),
            )
            if not is_created:
                c.title = city['fields']['title']
                c.title_en = city['fields']['title']
                c.title_ru = ru_translation
                c.save()

        for address in addresses:
            try:
                ru_translation = gs.translate(address['fields']['title'], 'ru')
                addr = Address.objects.get(id=address['pk'])
            except:
                continue
            addr.title = address['fields']['title']
            addr.title_en = address['fields']['title']
            addr.title_ru = ru_translation
            addr.save()

        self.stdout.write("Successfully complete\n")
