# coding=utf-8
__author__ = 'Slyfest'


from django.core.management.base import BaseCommand

import json
import os
import chardet
from django.db.utils import IntegrityError

from slugify import slugify
from support.exceptions import ApiStandartException
from places.models import Address, Place, Railway, Country, City, AddressCoord


class Command(BaseCommand):
    args = '<>'
    help = 'Geocode railways stations'

    def handle(self, *args, **options):

        with open(os.path.join(os.path.dirname(__file__), 'data', 'railways.json'), 'rw') as json_data:
            data = json.load(json_data)
            counter = len(data)
            for entry in data:
                if entry["model"] == "places.country":
                    Country.objects.get_or_create(
                        title=entry['fields']['title'],
                        slug=entry['pk'],
                        defaults=dict(
                            title_en=entry['fields']['title_en'],
                            in_profile=entry['fields']['in_profile'],
                            latitude=entry['fields']['latitude'],
                            longitude=entry['fields']['longitude'],
                        ),
                    )
                    counter -= 1
                    print counter
                if entry["model"] == "places.city":
                    City.objects.get_or_create(
                        country_id=entry['fields']['country'],
                        title=u'{0}'.format(entry['fields']['title']),
                        slug=entry['pk'],
                        defaults=dict(
                            title_en=u'{0}'.format(entry['fields']['title_en']),
                            latitude=entry['fields']['latitude'],
                            longitude=entry['fields']['longitude']
                        )
                    )
                    counter -= 1
                    print counter
                if entry["model"] == "places.addresscoord":
                    AddressCoord.objects.get_or_create(
                        latitude=entry['fields']['latitude'],
                        longitude=entry['fields']['longitude'],
                        defaults=dict(
                            pk=entry['pk'],
                            is_correct=entry['fields']['is_correct'],
                            city_id=entry['fields']['city'],
                        )
                    )
                    counter -= 1
                    print counter
                if entry["model"] == "places.address":
                    try:
                        Address.objects.get_or_create(
                            title=entry['fields']['title'],
                            address_coord_id=entry['fields']['address_coord'],
                            defaults=dict(
                                pk=entry['pk'],
                                title_en=entry['fields']['title_en']
                            )
                        )
                    except IntegrityError:
                        pass
                    counter -= 1
                    print counter

                if entry["model"] == "places.place":
                    try:
                        Place.objects.get_or_create(
                            title=entry['fields']['title'],
                            address_id=entry['fields']['address'],
                            defaults=dict(
                                pk=entry['pk']
                            )
                        )
                    except IntegrityError:
                        pass
                    counter -= 1
                    print counter
                if entry["model"] == "places.railway":
                    try:
                        Railway.objects.get_or_create(
                            pk=entry['pk'],
                            place_id=entry['fields']['place'],
                            uic=entry['fields']['uic']
                        )
                    except IntegrityError:

                        pass
                    counter -= 1
                    print counter