# coding=utf-8
__author__ = 'slyfest'

from django.core.management.base import BaseCommand
from django.db.models import Q
from ...models import *
from tzwhere import tzwhere
import shapely
import os
import json
from xhtml2pdf import pisa


def convert_html_to_pdf(source_html, output_filename):
    result_file = open(os.path.join(os.path.dirname(__file__), 'data', output_filename), 'w+b')
    template = file(os.path.join(os.path.dirname(__file__), 'data', source_html)).read()

    # convert HTML to PDF
    pisa_status = pisa.CreatePDF(
        template,  # the HTML to convert
        dest=result_file)  # file handle to recieve result

    # close output file
    result_file.close()  # close output file

    # return True on success and False on errors
    return pisa_status.err


class Command(BaseCommand):
    args = '<>'
    help = 'Geocodes the airports'

    def handle(self, *args, **options):
        self.stdout.write("Preparing db data for airports\n")

        # tz = tzwhere.tzwhere(shapely=True, forceTZ=True)
        # for city in City.objects.all():
        #     city_timezone = tz.tzNameAt(city.latitude, city.longitude)
        #     city.timezone = city_timezone
        #     city.save        #
        json_output = list()
        random_warehouse_list = Warehouse.objects.order_by('?')[:20]
        for warehouse in random_warehouse_list:

            operators = list()
            place_companies = PlaceCompany.objects.filter(place=warehouse.place)
            for place_company in place_companies:
                try:
                    if place_company.company:
                        operator = dict(
                            id=place_company.company.id,
                            title=place_company.company.name,
                            phones=place_company.company.phone,
                            email=place_company.company.email,
                            site=place_company.company.site,
                            logo='http://en.wikipilipinas.org/images/archive/3/3f/20070629015340!Airport_symbol.png',
                        )
                        operators.append(operator)
                except Company.DoesNotExist:
                    pass


            try:
                place_company = PlaceCompany.objects.filter(place=warehouse.place).distinct().first()
                place_type = place_company.service.type_service.type_place
                service_type = place_company.service.type_service.title
                json_object = dict(
                    id=warehouse.place.pk,
                    place_type=dict(
                        id=place_type.id,
                        title=place_type.title
                    ),
                    service_type=service_type,
                    address=dict(
                        city=warehouse.place.address.address_coord.city.title,
                        country=warehouse.place.address.address_coord.city.country.title,
                        latitude=warehouse.place.address.address_coord.latitude,
                        longitude=warehouse.place.address.address_coord.longitude
                    ),
                    services=[place_company.service.title],
                    title=warehouse.place.title,
                    info=dict(
                        operators=operators
                    )
                )
                json_output.append(json_object)
            except AttributeError:
                pass

        with open(os.path.join(os.path.dirname(__file__), 'data', 'warehouse_test.json'), 'w') as output:
            json.dump(json_output, output)
