# coding=utf-8
__author__ = 'slyfest'

from urllib2 import Request, urlopen
import urllib2
import json

from django.core.management.base import BaseCommand
from bs4 import BeautifulSoup
from orders.models import Bet
from support.push import send_bet_approve_push
from accounts.models import CustomUser, Device
from push_notifications.models import APNSDevice


def ports_parse(first_page, last_page, per_page):
    ports = list()
    counter = (last_page - first_page) * per_page
    for i in xrange(first_page, last_page):
        url = "http://www.marinetraffic.com/en/ais/index/ports/all/per_page:{0}/page:{1}".format(per_page, i)
        req = Request(url, headers={'User-Agent': 'Chrome'})
        try:
            raw = urlopen(req).read()
            soup = BeautifulSoup(raw, "lxml")
            table = soup.find("table", {"class": "table table-hover text-left"})
            for row in table.findAll("tr")[1:]:
                cells = row.findAll("td")
                try:
                    name = cells[1].a.string.strip()
                    unlocode = cells[2].string.strip()
                    ships_now = cells[6].a.string.strip()
                    departures = cells[7].a.string.strip()
                    arrivals = cells[8].a.string.strip()
                    expected = cells[9].a.string.strip()
                    capacity = int(ships_now) + int(departures) + int(arrivals) + int(expected)
                    counter -= 1
                    line = u'{0}:{1}:{2}    {3} left'.format(name, unlocode, capacity, counter)
                    port_data = dict(
                        name=name,
                        unlocode=unlocode,
                        capacity=capacity
                    )
                    ports.append(port_data)
                    print line
                except AttributeError:
                    pass
        except urllib2.HTTPError:
            pass
    return ports


class Command(BaseCommand):
    args = '<>'
    help = 'Parses marinetraffic.com'

    def handle(self, *args, **options):

        # bet = Bet.objects.get(pk=202)
        # send_bet_approve_push(bet)
        user = CustomUser.objects.get(pk=13)
        device = Device.objects.get(user=user)
        apns_device, created = APNSDevice.objects.get_or_create(registration_id=device.key)
        data = dict(
            info=dict(
                api_source=u'orders',
                message_push_type=1,
                msg_body=dict(
                    order_id=u'esghshjeshesheshesh',
                    title=u'sghesh',
                    msg=u'krhdrhdrhkik!!!111'
                )
            )
        )
        message = json.dumps(data)
        apns_device.send_message(message=u'{0}'.format(message), badge=1)


