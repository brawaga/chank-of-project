from django.core.management.base import BaseCommand
import countries


class Command(BaseCommand):
    args = '<>'
    help = 'Prepares the database'

    def handle(self, *args, **options):

        self.stdout.write("Preparing db data for 'countries' app\n")
        countries.add()

        self.stdout.write("Successfully complete\n")



