__author__ = 'Slyfest'

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PlacesConfig(AppConfig):
    name = 'places'
    verbose_name = _(u'Places')