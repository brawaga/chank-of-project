from django.contrib import admin
from django.contrib.admin.decorators import register
from modeltranslation.admin import TranslationAdmin

from .models import *


@register(TypePlace)
class AdminTypePlace(admin.ModelAdmin):
    list_display = ['id', 'title']


@register(TypeService)
class AdminTypeService(admin.ModelAdmin):
    list_display = ['id', 'title']


@register(AddressCoord)
class AdminAddressCoord(admin.ModelAdmin):
    list_display = ['id', 'latitude', 'longitude']


@register(Address)
class AdminAddress(TranslationAdmin):
    list_display = ['id', 'title']


@register(Country)
class AdminCountry(TranslationAdmin):
    list_display = ['slug', 'title', 'latitude', 'longitude']


@register(City)
class AdminCity(TranslationAdmin):
    list_display = ['slug', 'title', 'timezone', 'latitude', 'longitude']
    search_fields = ('slug',)


class AdminInlinePlaceCompany(admin.TabularInline):
    model = PlaceCompany
    fields = ('company', 'company_info', 'service')
    extra = 0


@register(Place)
class AdminPlace(admin.ModelAdmin):
    list_display = ['id', 'title', 'user_address']
    search_fields = ('user_address', )
    list_filter = ('places__service__type_service__type_place', )
    inlines = [AdminInlinePlaceCompany, ]


@register(Service)
class AdminService(admin.ModelAdmin):
    list_display = ['id', 'title', 'type_service']


@register(Airport)
class AdminAirport(admin.ModelAdmin):
    list_display = ['id', 'code_iata', 'code_ikao', 'phones', 'fax', 'site', 'rating']
    list_editable = ('rating',)
    raw_id_fields = ('place',)
    search_fields = ('code_iata', 'code_ikao')
    autocomplete_lookup_fields = {
        'fk': ['place'],
        'm2m': []
    }


@register(Company)
class AdminCompany(admin.ModelAdmin):
    list_display = ['id', 'name', 'user_address', 'phone', 'email', 'site']


@register(Port)
class AdminPort(admin.ModelAdmin):
    list_display = ['place', 'unlocode', 'rating']
    search_fields = ('unlocode',)
    list_editable = ('rating',)
    raw_id_fields = ('place',)
    autocomplete_lookup_fields = {
        'fk': ['place'],
        'm2m': []
    }


@register(Customs)
class AdminCustoms(admin.ModelAdmin):
    list_display = ['place', 'phone', 'fax', 'site', 'rating']
    list_editable = ('rating', )
    raw_id_fields = ('place', )
    search_fields = ('place__title', )
    autocomplete_lookup_fields = {
        'fk': ['place'],
        'm2m': []
    }


@register(Warehouse)
class AdminWarehouse(admin.ModelAdmin):
    list_display = ['id', 'place', 'rating']
    search_fields = ('place__title',)
    list_editable = ('rating',)
    raw_id_fields = ('place',)
    autocomplete_lookup_fields = {
        'fk': ['place'],
        'm2m': []
    }
