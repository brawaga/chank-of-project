# coding=utf-8
"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from wiki.urls import get_pattern as get_wiki_pattern
from django_nyt.urls import get_pattern as get_nyt_pattern

from django.contrib import admin

urlpatterns = i18n_patterns(
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^notifications/', get_nyt_pattern()),
    url(r'^wiki/', get_wiki_pattern()),

    url(r'^', include('vehicles.urls', namespace='vehicles')),
    url(r'^', include('profiles.urls'), name='profiles'),
    url(r'^', include('auxiliaries.urls')),
    url(r'^', include('accounts.urls')),
    url(r'^', include('company.urls')),
    url(r'^', include('points.urls')),
    url(r'^', include('places.urls')),
    url(r'^', include('chat.urls')),
    url(r'^', include('orders.urls'), name='orders'),
    url(r'^', include('payments.urls')),
    url(r'^', include('statistics.urls'), name='statistics'),
    url(r'^', include('transportations.urls'), name='transportations'),
]


