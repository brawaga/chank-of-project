#!/usr/bin/env bash
curl --trace feedback -H "Content-Type: application/json"  -X POST 'http://localhost:8000/api/v1/feedback/' \
    -d '{"email":"test@test.com", "message":"test message"}'
