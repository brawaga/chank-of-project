#!/usr/bin/env bash
curl --trace document -H "Content-Type: application/json" -H "Authorization: Bearer 68yG0rfXbJXkmuA6VAYGrs7X2vRLcY" -X POST 'http://localhost:8000/api/v1/profile/document/' \
    -d '{"passport_id": "123213", "iin": "31232123", "licence_id": "34143241"}' | python -mjson.tool