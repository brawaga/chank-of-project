# -*- coding: utf-8 -*-

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from transportila.celery import app

__author__ = 'sandan'


class SendEmail:

    def __init__(self):
        pass

    @staticmethod
    @app.task()
    def send(msg):
        data = msg['data']
        subject = render_to_string(data['subject_template'])

        letter_data = data['params']

        text_content = render_to_string(data['template_text'], letter_data)
        html_content = render_to_string(data['template_html'], letter_data)

        msg = EmailMultiAlternatives(subject, text_content, data['from_email'], data['emails_to'])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return u'Right send email to {0}'.format(str(data['emails_to']))



