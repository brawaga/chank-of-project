# -*- coding: utf-8 -*-

from transportila.celery import app

from transportations.models import Transportation

__author__ = 'sandan'


class SendingTransportationCertificates:

    def __init__(self):
        pass

    @staticmethod
    @app.task()
    def send(transportation_uuid):
        try:
            transportation = Transportation.objects.get(pk=transportation_uuid)
        except Transportation.DoesNotExist:
            # todo add logging
            return
        transportation.generate_certificates()

        return True



