import json
from django.conf import settings
from django.http import HttpResponse, Http404

__author__ = 'sandan'

from .exceptions import ApiStandartException


class ApiExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if isinstance(exception, ApiStandartException):
            try:
                error_code = exception.args[0]['error_code']
            except IndexError:
                return HttpResponse(status=500)
            error_message = exception.args[0]['error_message'] if 'error_message' in exception.args[0] else \
                str(settings.ERRORS_CODES[error_code])

            return HttpResponse(status=400, content=json.dumps(
                dict(
                    error_message=error_message,
                    error_code=error_code
                )
            ))
        else:
            return None


class WikiAccessMiddleware(object):

     def process_request(self, request):
        if '/wiki/' in request.path and not request.user.is_staff:
            raise Http404
