from rest_framework.serializers import Serializer, ModelSerializer
from .exceptions import ApiStandartException

__author__ = 'sandan'


class UnicodeErrorsMixin(object):

    def errors_to_unicode(self, errors):
        unicode_errors = dict()
        for title in errors:
            unicode_errors[title] = list()
            for index, item in enumerate(errors[title]):
                unicode_errors[title].append(unicode(item))
        return unicode_errors


class CustomSerializer(UnicodeErrorsMixin, Serializer):
    def is_valid(self, raise_exception=False):
        if not super(CustomSerializer, self).is_valid():
            raise ApiStandartException(dict(error_code=400001, error_message=self.errors_to_unicode(self.errors)))
        return True


class CustomModelSerializer(UnicodeErrorsMixin, ModelSerializer):
    def is_valid(self, raise_exception=False):
        if not super(CustomModelSerializer, self).is_valid():
            raise ApiStandartException(dict(error_code=400001, error_message=self.errors_to_unicode(self.errors)))
        return True
