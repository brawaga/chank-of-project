from profiles.models import BasicInfo, BillingInfo, DocumentInfo

__author__ = 'vol'


class Versioner:
    def __init__(self):
        pass

    @staticmethod
    def get_info(class_):
        if class_ == 'BasicInfo':
            return BasicInfo.objects
        elif class_ == 'BillingInfo':
            return BillingInfo.objects
        else:
            return DocumentInfo.objects

    @staticmethod
    def get_version(class_, data):
        info = Versioner.get_info(class_)
        previous_info = info.filter(user=data['user']).order_by(
            '-created_at')[:1]

        if previous_info:
            last_info = info.filter(user=data['user']).order_by('-version')[0]
            data['version'] = last_info.version + 1
        else:
            data['version'] = 1

        return data


class FieldFullness:
    obj = None

    def __init__(self, obj):
        self.obj = obj

    @staticmethod
    def delete_default_fields(fields):
        for_del_fields = ['id', 'created_at', 'user', 'user_id', 'confirmed']
        count = 0
        for field in for_del_fields:
            if field in fields:
                count += 1
                fields.remove(field)
        return count

    def get_fullness(self):
        if not self.obj:
            return 0
        fields = [f[0].name for f in self.obj._meta.get_concrete_fields_with_model()]
        self.delete_default_fields(fields)
        count = 0
        for field in fields:
            if eval('self.obj.{0}'.format(field)) is not None:
                count += 1
        try:
            return int(round(float(count) / len(fields), 2) * 100)
        except ZeroDivisionError:
            return 0


