# coding=utf-8
import os
from django.core.mail.message import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

__author__ = 'Slyfest'


def send_email_pdf(data):

    subject = render_to_string(data['subject_template'])

    letter_data = data['params']

    text_content = render_to_string(data['template_text'], letter_data)
    html_content = render_to_string(data['template_html'], letter_data)

    msg = EmailMultiAlternatives(subject, text_content, data['from_email'], data['emails_to'])
    msg.attach_alternative(html_content, "text/html")
    for item in data['pdf_paths']:
        msg.attach_file(item, "application/pdf")
    msg.send()


