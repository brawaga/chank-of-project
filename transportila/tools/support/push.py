# coding=utf-8
__author__ = 'Slyfest'

from push_notifications.models import GCMDevice, APNSDevice
from accounts.models import Device
import json


def send_gcm_message(message, device):
    gcm_device, created = GCMDevice.objects.get_or_create(registration_id=device.key)
    data = json.dumps(message)
    gcm_device.send_message(message=u'{0}'.format(data))


def send_apns_message(message, device):
    apns_device, created = APNSDevice.objects.get_or_create(registration_id=device.key)
    data = json.dumps(message)
    apns_device.send_message(None, badge=1, extra=dict(message=data))


def send_bet_approve_push(bet):
    transporter_device = Device.objects.get(user=bet.order_bettor.bettor)
    data = dict(
        info=dict(
            api_source=u'orders',
            message_push_type=2,
            msg_body=dict(
                order_id=str(bet.order_bettor.order.uuid),
                bet_id=bet.pk,
                title=u'Ответ Заказчику',
                sum=bet.cost,
                date_time=bet.created_at.strftime('%Y-%m-%d %H:%m'),
                msg=u'Вам необходимо ответить заказчику на его запрос'
            )
        )
    )
    if transporter_device.type == 1:
        send_gcm_message(data, transporter_device)
    elif transporter_device == 0:
        send_apns_message(data, transporter_device)


def send_bet_decline_push(bet, message, title):
    transporter_device = Device.objects.get(user=bet.order_bettor.bettor)
    data = dict(
        info=dict(
            api_source=u'orders',
            message_push_type=1,
            msg_body=dict(
                order_id=str(bet.order_bettor.order_id),
                title=title,
                msg=message
            )
        )
    )
    if transporter_device.type == 1:
        send_gcm_message(data, transporter_device)
    elif transporter_device == 0:
        send_apns_message(data, transporter_device)