__author__ = 'vol'
from rest_framework.exceptions import APIException
from django.utils.translation import ugettext_lazy as _


class EmailNotUniqueException(APIException):
    status_code = 409
    default_detail = _(u'This email address is already in use. Please supply a different email address.')


class NotUniqueException(Exception):
    pass


class UserIsNotActiveException(APIException):
    status_code = 403
    default_detail = _(u'This user not is active.')


class ApiStandartException(Exception):
    pass
