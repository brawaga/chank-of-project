#!/bin/bash
#
# Creates virtualenv
#

PROJECT_NAME="transportila"
REQUIREMENST_DIR="conf/env"

virtualenv --prompt="(${PROJECT_NAME}) " .virtualenv
source .virtualenv/bin/activate && pip install -r ${REQUIREMENST_DIR}/requirements.txt
